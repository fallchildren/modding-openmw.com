# Generated by Django 3.2.10 on 2023-12-23 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('momw', '0033_mod_special_to_spaces_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='datapath',
            name='manual',
            field=models.BooleanField(default=False),
        ),
    ]
