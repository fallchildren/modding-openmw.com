# Generated by Django 3.2.10 on 2024-06-21 19:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('momw', '0039_alter_tag_name_alter_tag_slug_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModArchive',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('direct_download', models.URLField(null=True)),
                ('extract_to', models.CharField(max_length=250)),
                ('for_mod', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mod_archives', to='momw.mod')),
                ('on_lists', models.ManyToManyField(blank=True, to='momw.ModList')),
            ],
            options={
                'verbose_name': 'Mod Archive',
                'verbose_name_plural': 'Mod Archives',
                'db_table': 'mod_archives',
                'ordering': ['pk'],
            },
        ),
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.CharField(max_length=50)),
                ('first_param', models.CharField(max_length=250)),
                ('second_param', models.CharField(max_length=250, null=True)),
                ('first_param_name', models.CharField(max_length=50)),
                ('second_param_name', models.CharField(max_length=50, null=True)),
                ('for_mod_archive', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='actions', to='momw.modarchive')),
                ('on_lists', models.ManyToManyField(blank=True, to='momw.ModList')),
            ],
            options={
                'verbose_name': 'Action',
                'verbose_name_plural': 'Actions',
                'db_table': 'actions',
                'ordering': ['pk'],
            },
        ),
    ]
