# Generated by Django 3.0.1 on 2020-01-08 03:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [("momw", "0020_remove_mod_step_num")]

    operations = [
        migrations.CreateModel(
            name="DataPathLoadOrder",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("order_number", models.IntegerField()),
                (
                    "mod",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT, to="momw.Mod"
                    ),
                ),
            ],
            options={
                "verbose_name": "Data Path Load Order",
                "verbose_name_plural": "Data Paths Load Order",
                "db_table": "datapath_loadorder",
                "ordering": ["pk"],
            },
        )
    ]
