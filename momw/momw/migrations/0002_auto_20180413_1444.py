# Generated by Django 2.0.1 on 2018-04-13 19:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [("momw", "0001_initial")]

    operations = [
        migrations.AlterField(
            model_name="mod",
            name="added_by",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL
            ),
        ),
        migrations.AlterField(
            model_name="mod",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT, to="momw.Category"
            ),
        ),
        migrations.AlterField(
            model_name="mod",
            name="step_num",
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="taggedmod",
            name="tag",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="momw_taggedmod_items",
                to="momw.Tag",
            ),
        ),
    ]
