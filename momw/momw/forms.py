from django import forms


class CfgGeneratorPresetForm(forms.Form):
    preset = forms.CharField(max_length=30, required=False)
