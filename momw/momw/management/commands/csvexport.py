import csv
import os
import sys

from datetime import datetime
from django.core.management.base import BaseCommand
from momw.models import ModList


class Command(BaseCommand):
    help = """
    Ouput a list as csv, requires a modlist slug as an argument. get the modlist slug from its url (e.g. https://modding-openmw.com/lists/i-heart-vanilla/ is I Heart Vanilla, so the slug is "i-heart-vanilla").
    """

    def add_arguments(self, parser):
        parser.add_argument("modlist_slug", type=str)

    def handle(self, *args, **options):
        slug = sys.argv[2]
        try:
            modlist = ModList.objects.get(slug=slug)
        except ModList.DoesNotExist:
            self.stderr.write(
                self.style.ERROR(
                    'The given mod list "{}" isn\'t an actual mod list!  Exiting...'.format(
                        slug
                    )
                )
            )
            sys.exit(1)

        now = datetime.now()
        outfile = "{n}-{s}.csv".format(n=now.strftime("%Y-%m-%d-%H:%M:%S"), s=slug)
        with open(outfile, "w") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(
                [
                    "LOAD ORDER",
                    "MOD",
                    "LINK",
                    "INCLUDED (yes/no)",
                    "AUTHOR",
                    "AUTHOR CONTACT",
                    "PERMISSION (yes/no)",
                ]
            )
            for mod in modlist.listedmod_set.all():
                if "href" in mod.mod.author:
                    author = mod.mod.author.split(">")[1].split("<")[0]
                else:
                    author = mod.mod.author

                writer.writerow(
                    [
                        mod.order_number,
                        mod.mod.name,
                        mod.mod.url,
                        "TBD",
                        author,
                        mod.mod.author if "href" in mod.mod.author else "N/A",
                        "TBD",
                    ]
                )

        self.stdout.write(
            self.style.SUCCESS("DONE! CSV output saved to: " + os.path.abspath(outfile))
        )
