#
# Parent list information
#
title = "Total Overhaul"
#
# Multi-line descriptions should be enclosed in triple quotes as seen below.
#
description = """<p>This list aims to replace virtually every mesh and texture in the game, as well as add a significant amount of content and gameplay tweaks. It will be like a remastered version of the game!</p>

<p>On this list you will find: high resolution and/or normal mapped textures, high resolution and/or optimized meshes (including atlased assets), major landmass and quest additions, body and animation changes, leveling and other related changes, and much more.</p>

<p>For a variant of this list that deals only with graphical mods, check out the <a href="/lists/graphics-overhaul/">Graphics Overhaul</a> list.</p>

<p>For a variant of this list that has all of the gameplay and content additions, but keeps the graphical mods to a minimum, check out the <a href="/lists/expanded-vanilla/">Expanded Vanilla</a> list.</p>

<div class="center"><iframe id='mod-list-utoob' src="https://www.youtube.com/embed/TQzCiYzfXP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>"""
short_description = "This list aims to replace virtually every mesh and texture in the game, as well as add a significant amount of content and gameplay tweaks. It will be like a remastered version of the game!"

#
# Define a sublist section with [[sublists]]
#
[[sublists]]
#
# Give the sublist a descriptive name. It will be automatically prefixed with "Parent Title: "
#
title = "First Steps"
description = "These are all the steps you need to follow before installing a mod list"
#
# Mods should be listed one per line, each line should end with a comma, and
# there must be opening and closing brackets enclosing the list as seen below.
#
mods = [
  "Buy and Install Morrowind",
  "Install OpenMW",
  "Set your User Settings",
  "Choose a Mod List",
  "Folder Deploy Script",
  "How to Install Mods",
]

#
# Add as many sublists as needed.
#
[[sublists]]
title = "Patches"
description = "Alter some aspect of the game in order to fix a bug, or to be more consistent with the game itself. Patches usually work via a plugin."

mods = [
  "Patch for Purists",
  "Expansion Delay",
  "Dubdilla Location Fix",
  "Greyed-out dialog",
]

#
# Add as many sublists as needed.
#
[[sublists]]
title = "Modding Resources"
description = "Community asset repositories, used by many other mods."
mods = [
  "Tamriel Data",
  "Tamriel_Data Graveyard - Deprecations un-deprecated",
  "OAAB_Data",
]

[[sublists]]
title = "Prerequisites"
description = "Mods that otherwise fit into other categories, but are placed here in order to prevent issues with others later in the list."
mods = [
  "Rise of House Telvanni",
  "White Suran 2 - MD Edition",
]

[[sublists]]
title = "Consistency"
description = "Mods that fix inconsistencies in the Morrowind game world."
mods = [
  "FMI - Nice to Meet You",
  "FMI - Caius Big Package",
  "FMI - NotAllDunmer",
  "FMI - Service Refusal and Contraband",
  "FMI - Sane Ordinators",
  "FMI - Hospitality Papers Expanded",
  "FMI - Legion Dialogue",
  "Expansion Resource Conflicts",
]

[[sublists]]
title = "Cut Content"
description = "Content that was promised by Todd, but never delivered, reimagined."
mods = [
  "Cutting Room Floor",
  "Artifacts Reinstated",
]

[[sublists]]
title = "Land & Landmass Additions"
description = "Add large amounts of extra land, including quests, to the game."
mods = [
  "Tamriel Rebuilt",
  "Skyrim: Home Of The Nords",
  "Solstheim Tomb of the Snow Prince",
]

[[sublists]]
title = "Meshes & Performance"
description = "Mesh replacers. Most of these correct mistakes in the originals, often resulting in performance increases, others will affect gameplay (Graphic Herbalism)."
mods = [
  "Mesh Fix v1.2",
  "Correct Meshes",
  "Morrowind Optimization Patch",
  "Graphic Herbalism - MWSE and OpenMW Edition",
  "Correct UV Rocks",
  "Properly Smoothed Meshes",
  "Project Atlas",
  "RR Mod Series - Better Meshes",
  "Jammings Off",
]

[[sublists]]
title = "Texture Packs"
description = "These provide large amounts of textures, covering all assets in the game or close to it."
mods = [
  "Morrowind Enhanced Textures",
  "OAAB Full Upscale",
  "Morrowind AIO normalmaps for OpenMW",
  "Normal Maps for Morrowind",
  "Normal maps for Morrowind - Tamriel Rebuilt - OAAB - Skyrim HotN - Stirk and other mods",
  "Normal Maps for Everything",
  "V.I.P. - Vegetto's Important Patches",
]

[[sublists]]
title = "Landscapes"
description = "High quality texture replacements for large amounts of the games landscapes."
mods = [
  "Tyddy's Landscape Retexture",
  "Landscape Retexture",
  "Apel's Asura Coast and Sheogorath Region Retexture",
  "Tamriel Rebuilt Terrain Normal Height Maps for OpenMW",
]

[[sublists]]
title = "Architecture"
description = "New meshes and textures for general architecture."
mods = [
  "Dwemer Mesh Improvement",
  "Dwemer Mesh Improvements Revamped",
  "Full Dwemer Retexture",
  "Full Dwemer Retexture - Normal Maps",
  "Dwemer Lightning Rods",
  "OAAB Dwemer Pavements",
  "Daedric Ruins - Arkitektora of Vvardenfell",
  "Daedric Ruins (Half-Revamped)",
  "Imperial Towns Revamp",
  "Imperial Forts Normal Mapped for OpenMW",
  "Shacks Docks and Ships - Arkitektora of Vvardenfell",
  "Sewers - Arkitektora of Vvardenfell",
  "Aesthesia - Stronghold textures",
  "Aesthesia - Stronghold Normal Maps",
  "Ghastly Glowyfence",
  "RR Mod Series - Morrowind Statues Replacer",
  "Baar Dau - Ministry of Truth",
  "Ministry of truth Bump mapped",
  "Ashlanders textures",
  "Vivec and Velothi - Arkitektora Vol.2",
  "Hlaalu - Arkitektora Vol.2",
  "Arkitektora White Suran",
  "Redoran - Arkitektora Vol.2",
  "Telvanni Mesh Improvement",
  "Telvanni - Arkitektora of Vvardenfell",
  "Necrom - Arkitektora of Morrowind",
  "Vivec and Velothi - Arkitektora Vol.2 Normal Maps",
  "Hlaalu - Arkitektora Vol.2 Normal Maps",
  "Redoran - Arkitektora Vol.2 Normal Maps",
]

[[sublists]]
title = "Flora & Foliage"
description = "Update the look of flora and foliage via improved meshes and high quality textures."
mods = [
  "Epic Plants",
  "Hackle-lo Fixed",
  "Thickle-Lo - The Succulent Hackle-Lo Mod",
  "Comberry Bush and Ingredient Replacer",
  "Pherim's Fire Fern - Plant and Ingredient",
  "Mid Mushroom Overhaul",
  "Vibrant Ivy and Trellis Additions",
  "Scum Retexture",
  "Scummy Scum",
  "Perfect Scum",
]

[[sublists]]
title = "Trees"
description = "Replace the stock trees with more dense and detailed assets."
mods = [
  "SM Bitter Coast Tree Replacer",
  "Melchior's Excellent Grazelands Acacia",
  "Vanilla-friendly West Gash Tree Replacer",
  "Remiros' Ascadian Isles Trees 2",
]

[[sublists]]
title = "Nature"
description = "Update the look various natural objects via improved meshes and high quality textures."
mods = [
  "Caverns Revamp",
  "Minor patches and fixes",
  "Apel's Fire Retexture Patched for OpenMW and Morrowind Rebirth",
  "Pimp my Lava",
  "Subtle Smoke",
  "Better Waterfalls",
  "Nocturnal Moths",
  "Fireflies",
  "Cave Drips",
]

[[sublists]]
title = "Environments"
description = "Update the visuals of stock objects in the environment."
mods = [
  "Road Marker Retextured",
  "Signposts Retextured",
  "Apel's Various Things - Signs",
  "Unique Tavern Signs for Tamriel Rebuilt",
  "Balmora Road Normal Maps",
  "Swayland",
]

[[sublists]]
title = "Sky & Weather"
description = "Alter the looks and behavior of the sky, clouds, and weather."
mods = [
  "Skies .IV",
  "Oh God Snow for Skies.iv and OpenMW",
  "New Starfields",
  "Dying Worlds - moons retexture",
  "Meteors",
  "Solthas Blight Weather Pack",
]

[[sublists]]
title = "Lighting"
description = "Update the behavior and look of lighting in the game, going for a darker and more accurate look overall."
mods = [
  "Glow in the Dahrk",
  "Nords shut your windows",
  "Magical lights for Telvanni",
  "Darknut's Lighted Dwemer Towers",
  "Improved Lights for All Shaders",
  "OpenMW vanilla candles patched with Enlightened Flames and ILFAS",
  "Dunmer Lanterns Replacer",
  "Logs on Fire",
]

[[sublists]]
title = "Clutter & Items"
description = "Updated assets for the very large number of clutter and other various items in the game."
mods = [
  "Remiros' Mod Graveyard",
  "Silverware Repolished",
  "Ingredients Mesh Replacer",
  "Detailed Tapestries",
  "AST Beds Remastered",
  "Better Kegstands",
  "Guar Skin Banners for OpenMW and Vanilla Morrowind",
  "Improved Kwama Eggs and Egg Sacs",
  "One True Faith - Saints and Frescoes Retexture",
  "Dunmeri Urns - Aestetika of Vvardenfell",
  "Long Live The Limeware - Retexture",
  "Long Live The Glassware - Retexture",
  "Long Live The Plates - Retexture",
  "Improved Better Skulls",
  "R-Zero's Random Retextures (and Replacers)",
  "EKM Vanilla-Based Paper Lanterns",
  "Ket's Potions and Beverages Retexture",
  "Ket's Swirlwood Furniture Retexture",
  "All Books Color-Coded and Designed",
  "OpenMW Containers Animated",
  "Apel's Various Things - Sacks",
  "Septim Gold and Dwemer Dumacs",
  "Crystal Soul Gems",
  "FM - Unique Items Compilation",
  "Know Thy Ancestors",
  "Dunmers ashpits",
  "Salts",
  "Transparent Shiny Bottles",
  "Store Entrance Chimes",
  "HD Forge",
  "6th House - Glowing Things",
  "HD Flags and Banners",
  "Better Telvanni Crystal",
  "Normal and Specular maps for Better Telvanni Crystal",
  "Temple Shrines Glow",
  "Pherim's Small and WIP Mods",
]

[[sublists]]
title = "Clothing"
description = "Higher quality clothing, and while we're at it, remove the enchanted glow \"effect\"."
mods = [
  "Better Clothes Complete",
  "Better Robes",
  "Better Robes - Updated Tamriel Rebuilt Patch",
  "New Gondolier Helm Fixed for OpenMW",
  "Common Shoe Pack",
]

[[sublists]]
title = "Armor"
description = "An updated look for the game's armor, with OpenMW's modern features in mind."
mods = [
  "HiRez Armors - Native Styles V2 Fixed and Optimized",
  "HiRez Armors Native Styles V2 for OpenMW",
  "Armors Retexture - Outlander Styles",
  "Daedric Lord Armor Morrowind Edition",
  "Orcish Retexture",
  "New Fists of Randagulf",
  "New Lord's Mail",
  "More Realistic Dwemer Armor",
  "Ebony Mail (Replacer)",
  "Glass Glowset",
  "More Glowing Mods",
  "Bear Armor Replacer",
]

[[sublists]]
title = "Weaponry"
description = "An update treatment for weapons, covering many uniques."
mods = [
  "Correct Iron Warhammer",
  "Sword and ring of nerevar",
  "SM The Tools of Kagrenac",
  "Oriental Mesh Improvements",
  "Weapon Sheathing",
  "Glass Glowset and Weapon Sheathing Patch",
  "Lysol's Steel Claymore Replacer",
  "Remiros' Uniques",
  "Rubber's Weapons Pack",
  "Eltonbrand Replacer",
  "Improved Thrown Weapon Projectiles",
  "New Widowmaker",
  "New Bow of Shadows",
  "Animated Staff of Magnus",
  "Skullcrusher Redone",
  "Daedric Crescent Replacer",
]

[[sublists]]
title = "Bodies And Heads"
description = "Replace the vanilla bodies and heads, and their textures, for all races and genders."
mods = [
  "Westly's Pluginless Head Replacer Complete",
  "Westly's Head and Hair Replacer - Hair Fix",
  "Westly's Faces Refurbished",
  "Westlys Master Head Pack Prim and Proper",
  "Facelift for Tamriel Data",
  "Khajiit Head Pack",
  "Whiskers Patch for MMH Version - All NPCs and Base Replace",
  "Better Bodies 3.2 (Better Beasts)",
  "New Beast Bodies - Clean Version",
]

[[sublists]]
title = "Creature Visuals"
description = "Give the various creatures of the game new life with updated assets."
mods = [
  "Divine Dagoths",
  "Rotat's Corprus Natives Replacer",
  "Luminous Atronachs",
  "Westly Presents: Unique Winged Twilights",
  "Netch Bump Mapped",
  "Silt Strider",
  "Silt Strider recolor and clutter normalmaps for OpenMW",
  "Silt Strider Animation Restored",
  "Mudcrab Replacer",
  "Cliff Racer Replacer 2.0",
  "4thUnknowns Creatures Morrowind Edition",
  "Blighted Animals Retextured",
  "Blighted 4thUnknown's Beasts Retextured",
  "Better Spriggans",
  "Scamp Replacer",
  "Hunger Replacer",
  "SM Bonewalker Replacer",
  "FMI - Unique Lore Friendly Cave Rats",
]

[[sublists]]
title = "VFX"
description = "Updated visual effects. Greatly enhance many scenes with subtle but beautiful additions."
mods = [
  "Mistify",
  "Remiros' Minor Retextures - Mist Retexture",
  "The Dream is the Door",
  "Parasol Particles",
  "Magic VFX Retexture by Articus",
  "Spells Reforged",
  "No Shield Sparkle",
  "Diverse Blood",
  "Improved Propylon Particles",
]

[[sublists]]
title = "Animations"
description = "Update the game's dated animations with improved assets."
mods = [
  "Simply Walking (Remastered)",
  "MCAR",
  "ReAnimation v2 - Rogue - first-person animation pack",
]

[[sublists]]
title = "Audio"
description = "Mods that add or change sounds or music."
mods = [
  "Same Low Price Fix",
  "MAO Spell Sounds",
  "Audiobooks of Morrowind",
]

[[sublists]]
title = "Exteriors"
description = "Updates of existing exterior locations, making them more varied by incoporating new assets and adding new points of interest."
mods = [
  "OAAB - Foyada Mamaea",
  "OAAB - The Ashen Divide",
  "Concept Art Molag Amur Region - The Great Scathes",
  "The Mountain of Fear",
  "Bal'laku - The Lonely Towers",
  "Tales from the Ashlands - The Great Hive Baan Binif",
  "Gates of Ascadia",
  "The Great Seawall of Vivec",
  "Vivec Lighthouse",
  "Dagon Fel Lighthouse",
  "Little Landscapes - Bitter Coast Waterway",
  "Little Landscapes - Path to Pelagiad",
  "Compatible Odai River Upper Overhaul",
  "Little Landscapes - Path to Vivec Lighthouse",
  "Little Landscapes - Seyda Neen Swamp Pools",
  "Little Landscape - Path to Balmora",
  "Little Landscape - Foyada of Sharp Teeth",
  "The Grove of Ben'Abi",
  "The Grove of Ben'Abi Enhanced",
  "OAAB Pomegranates",
  "Hidden Sea Loot",
  "Telvanni Sea Beacons",
  "Marbled Zafirbel Bay",
  "Justice for Khartag (J.F.K.)",
  "OAAB Shipwrecks",
  "Holamayan Island",
  "Sanctus Shrine",
  "Immersive Grotto Entrances",
]

[[sublists]]
title = "Groundcover"
description = "Add grass, reeds, small mushrooms, stones, and other groundcover to the game."
mods = [
  "Lush Synthesis",
  "Vurt's Groundcover for OpenMW",
  "Remiros' Groundcover",
  "Remiros Groundcover Textures Improvement",
  "OAAB Saplings",
  "Grass for Tamriel Rebuilt v23.1",
]

[[sublists]]
title = "Cities & Towns"
description = "These make cities and settlements more beautiful by updating existing content or adding great new content of their own."
mods = [
  "Beautiful Cities of Morrowind",
  "Maar Gan - Town of Pilgrimage",
  "Nordic Dagon Fel",
  "Hanging Gardens of Suran",
  "Velothi Wall Art",
  "Concept Art Palace (Vivec City)",
  "Better Flames for Concept Art Palace (Vivec City) - OpenMW Version",
  "Guar Stables of Vivec",
  "Bell Towers of Vvardenfell",
  "Bell Towers of Vvardenfell Directional Sound for OpenMW",
  "Planters for Ajira",
  "RR Mod Series - Telvanni Lighthouse Tel Branora",
  "RR Mod Series - Telvanni Lighthouse Tel Vos",
  "Concept Arts plantations",
  "OAAB Grazelands",
  "OAAB Tel Mora",
  "Sadrith Mora BCOM Plus Module",
  "The Wolverine Hall",
  "Caldera Governors Manor Redone",
  "Immersive Mournhold",
]

[[sublists]]
title = "Interiors"
description = "Updates of existing interior cells in order to make them feel more lived in and expansive."
mods = [
  "Almalexia's Chamber Overhaul",
  "Nordic Solstheim - Solstheim Interiors Overhaul",
  "Dagoth Ur Welcomes You",
  "Akulakhan's best chamber",
  "Azura's Shrine Overhaul",
  "Library of Vivec Enhanced",
  "Ald'ruhn-under-Skar",
  "Ald'Ruhn Manor Banners",
  "Tamriel Rebuilt - Hall of Justice Overhaul",
]

[[sublists]]
title = "Caves & Dungeons"
description = "Mods that add, enhance, and/or replace various caves and/or dungeons."
mods = [
  "OAAB - Tombs and Towers",
  "Mines and Caverns",
  "New Ilunibi",
  "The Corprusarium experience",
  "Of Eggs and Dwarves - Gnisis Eggmine and Bethamez Overhaul",
  "What Kwamas Forage - An Eggmine Mod",
  "Dungeon Details Abaesen-Pulu Egg Mine",
  "OAAB - Hawia Egg Mine",
  "Better Dunmer Strongholds",
  "Berandas Overhaul",
  "Samarys Ancestral Tomb Expanded",
  "Andrano Ancestral Tomb Remastered",
  "Andrethi Tomb Overhaul",
  "Drethos Ancestral Tomb",
  "Daedric Shrine Overhaul FULL",
  "Shal Overgrown",
  "Adanumuran Reclaimed",
  "Mamaea Awakened",
  "Fadathram",
  "Vennin's Ulummusa Overhaul",
  "Vennin's Pulk Overhaul",
  "Face of the Hortator",
]

[[sublists]]
title = "NPCs"
description = "Add more variety and (hopefully) immersion by updating the rather limited NPC scope of the base game."
mods = [
  "Repopulated Morrowind",
  "NOD - NPC Outfit Diversity",
  "Yet Another Guard Diversity",
  "Yet Another Guard Diversity Expanded Imperials",
  "Nordic Dagon Fel NPCs",
  "Bards of Bardenfell",
  "Diverse Khajiit - New and Improved",
  "Sload and Slavers",
  "Sloadic Transports",
  "Better Almalexia",
  "VEHK - Concept art Vivec replacer",
  "By Azura",
  "Umbra - Blademaster",
  "Wandering Umbra",
  "Royal Barenziah",
  "Interesting Outfits - Solstheim",
  "Shady Sam",
  "M'Aiq on the Mainland and other mods",
]

[[sublists]]
title = "Companions"
description = "Adds companions to join you on your adventures."
mods = [
  "Attend Me",
  "Blademeister",
]

[[sublists]]
title = "Dialogue"
description = "Tweaks and enhances the NPC dialogue in various ways, and adds new voiced lines."
mods = [
  "Voice Overhaul",
  "Quest Voice Greetings",
  "Idle Talk",
  "Dagoth Ur Voice addon v10",
  "Almalexia Voice",
  "Vivec Voice Addon Tribunal Version",
  "Nastier Camonna Tong",
  "No More Stage Diving - Desele's Dancing Girls",
  "Greetings for No Lore",
  "FMBP - Greet Service",
  "LDM - Context Matters",
  "Djangos Dialogue 1.4",
]

[[sublists]]
title = "Balance & Nerfing"
description = "These mods try to bring some balance through changes to the game mechanics and world."
mods = [
  "MDMD - More Deadly Morrowind Denizens",
  "Beware the Sixth House (Sixth House Overhaul)",
  "Tribunal Rebalance",
  "Bloodmoon Rebalance",
  "Speechcraft Rebalance (OpenMW)",
  "Alchemical Hustle",
  "For the Right Price",
  "Ownership Overhaul",
  "Real Disposition",
  "Races RESPECted - A Race Skill and Attribute Rebalance Mod",
  "Birthsigns RESPECted - A Birthsign Rebalance Mod",
  "Fatigue and Speed and Carryweight Rebalance (OpenMW)",
  "Pickpocket Rebalance (OpenMW)",
  "Spell Effects Rebalance",
  "Better Blight",
  "Immersive Telvanni Bug Musk",
]

[[sublists]]
title = "Creatures"
description = "Adds a variety of new passive and hostile creatures to the game world, and rebalances the vanilla game critters."
mods = [
  "Vanilla friendly creatures and undeads expansion",
  "Riekling Fix and Expansion",
  "Winged Twilight Rebalance",
  "Atronach Rebalance",
]

[[sublists]]
title = "Gameplay"
description = "Update gameplay mechanics. From leveling to monster spawning, marksman to sneaking, this selection tries to update game mechanics to feel better or more enjoyable compared to the vanilla game."
mods = [
  "Expansions Integrated",
  "At Home Alchemy - Finished",
  "ZEF Focus Magicka",
  "NCGDMW Lua Edition",
  "Solthas Combat Pack (OpenMW Lua)",
  "Clear Your Name",
  "Faction Service - Beds",
  "Lower First Person Sneak Mode",
  "Projectile Overhaul - Modular",
  "Smart Ammo for OpenMW-Lua",
  "TimeScale Change - OpenMW",
  "Quill of Feyfolken - Scroll Enchanting",
  "Zack's Lua Multimark Mod",
  "Nighttime Door Locks",
  "No more Dr. Nerevarine",
  "Pursuit (OpenMW)",
  "Wandering Creature Merchants",
  "Pharis' Magicka Regeneration",
  "Practical Necromancy (OpenMW)",
  "Light Hotkey",
  "Protective Guards (OpenMW)",
  "Shield Unequipper",
  "NoPopUp Chargen",
]

[[sublists]]
title = "New Equipment"
description = "Adds a variety of new equipment and items to the game world and traders."
mods = [
  "Wares Ultimate",
  "Stuporstar's Books for Wares",
  "OAAB Integrations",
  "Solthas Sixth House Amulet",
  "Mage Robes",
  "Telvanni Magister Robes",
  "Hircine's Artifacts",
  "The Madstone",
  "SM Mask of Dagoth Ur",
  "Adventurer's Backpacks",
  "The Forgotten Shields - Artifacts",
  "Argonian Full Helms Lore Integrated and Modders Resource v 1.1",
  "Boots for Beasts - Amenophis Revision",
  "Daedric Maul",
]

[[sublists]]
title = "Quests"
description = "Add new quests, alter existing areas, get new rewards. Includes new content from some of the developers of the original Morrowind!"
mods = [
  "AFFresh",
  "Fargoth Says Hello",
  "The Mananaut's Message",
  "Master Index Redux",
  "The Doors of Oblivion",
  "The Doors of Oblivion Full Upscale",
  "Secrets of the Crystal City",
  "Glowing Secrets of the Crystal City",
  "Greymarch Dawn - Whispers of Jyggalag",
  "Sharper Normal Maps for Greymarch Dawn - Whispers of Jyggalag",
  "Caldera Mine Expanded",
  "The Search for the White Wave",
  "Aspect of Azura",
  "Investigations at Tel Eurus",
  "Lord of Rebirth",
  "Frozen in Time",
  "Aether Pirate's Discovery",
  "Silent Island",
  "Mudcrab Imports",
  "Caldera Priory and the Depths of Blood and Bone",
  "Terror of Tel Amur",
  "A Cold Cell",
  "Galen's Quest for Truth",
  "The Plague Doctor",
  "Ebonheart Underworks",
  "The Mysterious Affair of Sara Shenk",
  "Roaring Arena - Betting and Bloodletting",
  "Early Transport to Mournhold",
  "Tamriel Rebuilt Introduction Quest - Help a Khajiit reach the City of Good People",
  "Keelhouse - A quest and house mod for Tamriel Rebuilt",
  "The Patchwork Airship - Fleshing out a vanilla quest",
  "Immersive Imperial Skirt",
  "Gondolier's License",
  "Comfy Pillow for a restful sleep",
  "Wealth Within Measure",
  "Uncharted Artifacts",
  "Sorcerer of Alteration",
  "Something in the Water -- A Peryite Daedric Quest",
  "Questline - Vivec Lighthouse Keeper",
  "Ancient Foes",
]

[[sublists]]
title = "Factions"
description = "Adds new content and quests for the original game's factions, and introduces a couple of never before seen factions with full questlines."
mods = [
  "Imperial Legion Expansion",
  "Imperial Factions",
  "Census and Excise Office Faction",
  "Astrologian's Guild",
  "Join the Dark Brotherhood - Dark Brotherhood Faction Mod",
  "Quarra Clan - Refreshed",
  "Quests for Clans and Vampire Legends (QCVL)",
  "Temple Master",
  "Ranis Athrys - Let Go and Begin Again",
  "OAAB Brother Junipers Twin Lamps",
  "Telvanni Staff for the Telvanni Staff",
  "Wizard's Staff for Wizards",
]

[[sublists]]
title = "The Player Home"
description = "The fabled Player Home genre of mod! Everybody loves a free mansion, right?"
mods = [
  "Fargoth's Mountain Hut",
  "Fabulous Hlaalo Manor",
  "Draggle-Tail Shack",
  "The Archmagister's Abode",
  "Uvirith's Legacy",
  "Building Up Uvirith's Legacy",
  "Mages Guild Stronghold - Nchagalelft",
  "Rather Nice Factor's Estate",
]

[[sublists]]
title = "Randomizers"
description = "Mods that randomize some aspect of the game. Not recommended if this is your first time playing the game!"
mods = [
  "mtrDwemerPuzzleBoxRandomizer - Randomizer for Location of Dwemer Puzzle Box within Arkngthand associated with Main Quest",
  "Oh No Stolen Reports",
  "Farvyn Oreyn's Position Randomizer",
  "Din's Position Randomizer",
  "Suran Dancer Randomized",
]

[[sublists]]
title = "Distant Details"
description = "Mods that affect object and visuals in the distance."
mods = [
  "Dynamic Distant Buildings for OpenMW",
  "Distant Fixes: Rise of House Telvanni",
  "Distant Fixes: Uvirith's Legacy",
]

[[sublists]]
title = "Water"
description = "Modify the water or sea in some shape or form."
mods = [
  "Distant Seafloor for OpenMW",
]

[[sublists]]
title = "Books"
description = "Mods that add specific books."
mods = [
  "An N'wah's Guide To Modern Culture And Mythology",
]

[[sublists]]
title = "Mod Patches"
description = "Collections of small patches and other plugins for compatibility between various mods, or between various mods and OpenMW."
mods = [
  "MOMW Patches",
  "Sophie's Skooma Sweetshoppe",
  "Alvazir's Various Patches",
  "Khajiit Has Patches If You Have Coin",
  "Sigourn's Misc Mods and Patches",
  "Mono's Minor Moddities",
  "Publicola's Misc Mod Emporium",
  "Animated Morrowind and Weapon Sheathing patch for OpenMW",
  "Various tweaks and fixes",
]

[[sublists]]
title = "Camera"
description = "Mods that affect the camera in one way or another."
mods = [
  "Action Camera Swap",
]

[[sublists]]
title = "Post Processing Shaders"
description = "Post processing shaders add a variety of nice visual effects."
mods = [
  "MOMW Post Processing Pack",
]

[[sublists]]
title = "User Interface"
description = "Give the game UI a needed facelift in various ways."
mods = [
  "Monochrome User Interface",
  "Enchanting icons as you wish",
  "Gonzo's Splash Screens",
  "Alternative TrueType Fonts",
  "Big Icons",
  "Vvardenfell Animated Main Menu",
  "Simple HUD for OpenMW (with compass or minimap)",
  "Small Skyrim Crosshair (OpenMW compatible)",
]

[[sublists]]
title = "Movies"
description = "Higher quality in-game cinematics."
mods = [
  "HD Intro Cinematic - English",
]

[[sublists]]
title = "Dev Build Only"
description = "Mods that require the latest OpenMW Developer Build."
mods = [
  "Perfect Placement",
  "Remove Negative Lights for OpenMW",
  "Dynamic Music (OpenMW Only)",
  "Dynamic Music - Muse Music Expansion Hlaalu Compatibility (OpenMW Only)",
  "Dynamic Music - Muse Music Expansion Ashlander Compatibility (OpenMW Only)",
  "Dynamic Music - Muse Music Expansion Redoran Compatibility (OpenMW Only)",
  "MUSE Music Expansion - Hlaalu",
  "MUSE Music Expansion - Ashlander",
  "MUSE Music Expansion - Redoran",
  "Tamriel Rebuilt - Original Soundtrack",
  "MUSE Music Expansion - Sixth House",
  "MUSE Music Expansion - Daedric",
  "MUSE Music Expansion - Dwemer",
  "MUSE Music Expansion - Empire",
  "MUSE Music Expansion - Tomb",
  "Vindsvept Solstheim for Dynamic Music (OpenMW)",
  "Songbook of the North - A music mod for Skyrim Home of the Nords",
  "Signpost Fast Travel",
  "Guards' Hounds",
  "Devilish Vampire Overhaul",
  "No Witness - No Bounty",
  "Harvest Lights",
  "Weather Particle Occlusion",
  "No Sneaky Sleeping",
  "Corporeal Carryable Containers",
  "Quicktrain",
  "OpenMW Skyrim Style Quest Notifications",
  "Most Wanted Nerevarine",
  "Convenient Thief Tools",
  "Mage Robes for OpenMW-Lua",
  "HD Forge for OpenMW-Lua",
  "Bound Balance",
  "Audiobooks of Tamriel Rebuilt",
]

[[sublists]]
title = "Settings Tweaks"
description = "Not actually mods, but changes to settings that are built into the OpenMW engine. Enhance performance and visual quality, as well as enable or disable many gameplay options (including things provided by MCP for vanilla Morrowind)."
mods = [
  "True Nights and Darkness",
  "Distant Land And Objects",
  "Shadows",
  "Water",
  "MOMW Camera",
  "MOMW Gameplay",
]

[[sublists]]
title = "Merging"
description = "The final bits of work, to ensure long-term smooth sailing."
mods = [
  "Delta Plugin and Groundcoverify",
  "waza_lightfixes",
]
