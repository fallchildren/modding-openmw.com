import json

from momw.models import ModArchive, Mod, ModList


def mod_archives() -> bool:
    data = []
    with open("momw/momw/data_seeds/data/mod-archives.json", "rb") as f:
        data = json.load(f)

    for arch in data:
        x = ModArchive()
        x.extract_to = arch["extract_to"]
        x.name = arch["name"]
        x.direct_download = arch.get("direct_download")
        x.for_mod = Mod.objects.filter(name=arch["for_mod"]).first()
        x.save()

        for modlist in arch["on_lists"]:
            x.on_lists.add(ModList.objects.filter(slug=modlist).first())

        x.save()
    return True
