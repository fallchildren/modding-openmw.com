import json

from momw.models import Action, ModArchive, ModList


def actions() -> bool:
    data = []
    with open("momw/momw/data_seeds/data/actions.json", "rb") as f:
        data = json.load(f)

    for a in data:
        x = Action()
        x.action = a["action"]
        x.first_param = a["first_param"]
        x.first_param_name = a["first_param_name"]
        if a.get("second_param"):
            x.second_param = a["second_param"]
            x.second_param_name = a["second_param_name"]
        x.for_mod_archive = ModArchive.objects.filter(
            name=a["for_mod_archive"], for_mod__name=a["for_mod"]
        ).first()
        x.save()

        for modlist in a["on_lists"]:
            x.on_lists.add(ModList.objects.filter(slug=modlist).first())

        x.save()
    return True
