import sys

from momw.helpers import read_toml_data
from momw.models import Mod, ModList, UsageNotes


def usage_notes():
    data = read_toml_data("usage-notes")
    for n in data["usage_notes"]:
        try:
            m = Mod.objects.get(name=n["for_mod"])
        except Mod.DoesNotExist:
            print("ERROR: mod for note does not exist!", n["for_mod"])
            sys.exit(1)

        note = UsageNotes(**{"text": n["text"], "for_mod": m})
        note.save()

        if "on_lists" in n.keys():
            mod_lists = []
            for slug in n["on_lists"]:
                try:
                    mod_lists.append(ModList.objects.get(slug=slug))
                except ModList.DoesNotExist:
                    if slug == "generic":
                        note.generic = note.text
                        note.save()
                    else:
                        print(f"ERROR: mod list for note does not exist: {slug}")
                        sys.exit(1)
            if mod_lists:
                for l in mod_lists:
                    note.on_lists.add(l)
