import sys

from momw.helpers import read_yaml_data
from momw.models import ListedMod, Mod, ModList, DataPath


def data_paths():
    """
    Read a YAML file for a list of mod names to assemble a global data path
    load order based on DataPathLoadOrder objects.
    """
    ihvdc = ModList.objects.get(slug="i-heart-vanilla-directors-cut")
    modlist_map = {
        "i-heart-vanilla": ModList.objects.get(slug="i-heart-vanilla"),
        "i-heart-vanilla-dc": ihvdc,
        "i-heart-vanilla-directors-cut": ihvdc,
        "one-day-morrowind-modernization": ModList.objects.get(
            slug="one-day-morrowind-modernization"
        ),
        "graphics-overhaul": ModList.objects.get(slug="graphics-overhaul"),
        "total-overhaul": ModList.objects.get(slug="total-overhaul"),
        "expanded-vanilla": ModList.objects.get(slug="expanded-vanilla"),
        "starwind-modded": ModList.objects.get(slug="starwind-modded"),
        "just-good-morrowind": ModList.objects.get(slug="just-good-morrowind"),
    }

    order_data = read_yaml_data("data-path-order")

    count = 1
    for d in order_data:
        try:
            mod = Mod.objects.get(name=d["for_mod"])

            dp = DataPath(
                **{
                    "for_mod": mod,
                    "order_number": count,
                }
            )

            if "extra_dirs" in d:
                dp.extra_dirs = d["extra_dirs"]

            if "dev_build" in d:
                dp.dev_build = d["dev_build"]

            if "manual" in d:
                dp.manual = d["manual"]

            if "for_cfg" in d:
                dp.for_cfg = d["for_cfg"]

            dp.save()

            if "on_lists" in d:
                on_lists = []

                for slug in d["on_lists"]:
                    # Is the mod for this data path actually on the given mod list?!
                    if mod.name != "Morrowind" and (
                        ListedMod.objects.filter(
                            mod=mod, modlist__slug__startswith=slug
                        ).count()
                        < 1
                    ):
                        print(
                            f"ERROR: The data path '{dp.linux}' isn't used with mod '{mod}' on mod list '{modlist_map[slug].title}'"
                        )
                        sys.exit(1)

                    try:
                        on_lists.append(modlist_map[slug])
                    except KeyError:
                        print("ERROR: modlist for data path doesn't exist:", slug)
                        sys.exit(1)

                for l in on_lists:
                    dp.on_lists.add(l)

            count += 1

        except Mod.DoesNotExist:
            print("ERROR: mod for data path doesn't exist:", d["for_mod"])
            sys.exit(1)
