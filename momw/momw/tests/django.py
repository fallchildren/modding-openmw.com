from django.contrib.auth.models import AnonymousUser
from django.shortcuts import reverse
from django.test import RequestFactory, TestCase
from ..models import Mod, Category, Tag
from ..views.utility import static_view

HTTP_OK = 200


class MomwTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.cat1 = Category(title="Test Category 1", slug="test-category-1")
        cls.tag1 = Tag(name="TEST TAG1", slug="test-tag-1")
        cls.mod1 = Mod(
            name="Test Mod 1",
            author="Test Mod Author",
            description="Test Mod 1 Description",
            category=cls.cat1,
            is_active=True,
            # compat_link="dafuq",
            # alt_to
            date_added="2023-09-02 11:57:00 -0500",
            date_updated="2023-09-02 11:57:00 -0500",
            dl_url="DL URL FIELD",
            tags=[cls.tag1],
            slug="test-mod-1",
            status=1,
        )

    def setUp(self):
        self.f = RequestFactory()

    def test_cat_get_absolute_url(self):
        self.assertEqual(
            self.cat1.get_absolute_url(), "/mods/category/test-category-1/"
        )

    def test_mod_clean_name(self):
        self.assertEqual(self.mod1.clean_name, "TestMod1")

    def test_mod_compat_string(self):
        self.assertEqual(self.mod1.compat_string, "Fully Working")

    def test_mod_compat_link(self):
        self.assertEqual(
            self.mod1.compat_link,
            '<a href="/compatibility/#fully-working">Fully Working</a>',
        )

    def test_mod_dl_item(self):
        self.assertEqual(self.mod1.dl_item(), "DL URL FIELD")

    def test_mod_get_absolute_url(self):
        self.assertEqual(self.mod1.get_absolute_url(), "/mods/test-mod-1/")

    def test_mod_get_status_string(self):
        self.assertEqual(self.mod1.get_status_string(), "Live")

    def test_tag_get_absolute_url(self):
        self.assertEqual(self.tag1.get_absolute_url(), "/mods/tag/test-tag-1/")

    def test_about_page(self):
        r = self.f.get(reverse("about"))
        r.user = AnonymousUser()
        response = static_view(r, "about.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_changelogs_website(self):
        r = self.f.get(reverse("changelogs-website"))
        r.user = AnonymousUser()
        response = static_view(r, "changelogs-website.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_index(self):
        r = self.f.get(reverse("index"))
        r.user = AnonymousUser()
        response = static_view(r, "index.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_privacy_page(self):
        r = self.f.get(reverse("privacy"))
        r.user = AnonymousUser()
        response = static_view(r, "privacy.html")
        self.assertEqual(response.status_code, HTTP_OK)
