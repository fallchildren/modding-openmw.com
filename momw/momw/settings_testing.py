from .settings_server import *  # NOQA

MOMW_VER = "testing"
PROJECT_HOSTNAME = "testing.modding-openmw.com"
SITE_NAME = "testing.Modding-OpenMW.com"
USE_ROBOTS = True
SERVER_EMAIL = f"app@{SITE_NAME}"
