from django.shortcuts import render


def csrf_failure_view(request, reason):
    return render(request, "errors/csrf_failure.html", {"reason": reason})
