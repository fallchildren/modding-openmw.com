{% extends 'base.html' %}
{% block meta %}
  <meta property="og:site_name" content="Modding-OpenMW.com" />
  <meta property="og:type" content="website">
  <meta property="og:url" content="{% url 'guides-users' %}">
  <meta property="og:title" content="Users' Guide">
  <meta property="og:description" content="How do you use this website? How do you get the most out of all the information presented here? This page should answer those questions and more!">
  <meta property="og:image" content="https://modding-openmw.com/favicon.png">
{% endblock %}
{% block title %}Users' Guide{% endblock %}
{% block content %}
  <h1>Users' Guide</h1>

  <p>
    How do you use this website? How do you get the most out of all the information presented here? This page should answer those questions and more!
  </p>

  <h2 id="quickstart"><a href="#quickstart">Quickstart</a></h2>

  <p>Below is a quick, high-level overview of this website's workflow:</p>

  <ol>
    <li><a href="{% url 'gs-buy' %}">Purchase Morrowind</a> if needed</li>
    <li>Install Morrowind and <a href="#buy-morrowind">OpenMW</a></li>
    <li>Successfully run vanilla Morrowind via OpenMW with no mods or settings changes</li>
    <li>Configure your mod and Morrowind install paths on <a href="{% url 'user_settings' %}">the user settings page</a>. You can also opt into mods and mod features that require an OpenMW Dev Build</li>
    <li>Choose a mod list (each are listed and described <a href="#choose-mod-list">below</a>)</li>
    <li>Run <a href="#folder-deploy-script">the "folder deploy" script</a>, which will create folders for each mod in your list and any additional folders that might otherwise need to be manually made</li>
    <li>Check out our <a href="{% url 'example-mod-page' %}">fully annotated example mod detail page</a> to learn how to read the information on each mod page</li>
    <li><span class="bold">We DO NOT recommend using mod managers like Mod Organizer 2 with this website's mod lists.</span> For basic users, Mod Organizer 2 simply takes more time and effort to set up properly than by just following the instructions on this website. Refer to <a href="{% url 'faq-tooling' %}">our Tooling FAQ</a> to read more.</li>
    <li>Begin the mod list you've chosen:
      <ul>
        <li>Go through each mod, one at a time</li>
        <li>Use the <code>Download Link</code> to download the mod</li>
        <li>Use an archive extractor tool such as 7zip to extract the files to their designated directory provided by the <code>Data Paths</code> on each page.</li>
        <li>Keep an eye out for any additional files you must download or extra instructions you must follow documented in the <code>Usage Notes</code> on some pages</li>
        <li>Sometimes a mod's description on Nexus may have different usage information than our usage notes -- please stick with what we advise. This is because many of the mod lists on this website are large and complex and may require different instructions for everything to work together. <span class="bold">It is vitally important that you DO NOT enable every plugin from every mod!</span></li>
        <li>Click on the <code>Enable For CFG Generator</code> button after you've installed a mod and move onto the next mod in the list</li>
        <li>At any time, you can visit the <a href="{% url 'cfg_generator3' %}">CFG Generator</a> page to get a customized cfg that matches your progress (handy for testing as you go, more on that <a href="#testing-your-progress">below</a>)</li>
        <li>Once you are finished with a mod list, you will be taken to its 'Final Steps' page which will instruct you on using the <a href="{% url 'cfg_generator3' %}">CFG Generator</a> and also the tools you need to run before you can start playing</li>
      </ul>
    </li>
    <li>The command-line tools you'll need to use are listed below:
      <ul>
        <li>The mod lists on this website require you to manually clean some plugins with <a href="{% url 'tips-cleaning' %}">tes3cmd</a></li>
        <li>One of the final steps will be to run <a href="{% url 'mod_detail' slug='delta-plugin' %}">DeltaPlugin</a> to produce a merged objects plugin</li>
        <li>Many of our lists use <a href="{% url 'mod_detail' slug='groundcoverify' %}">Groundcoverify</a> to produce a plugin and assets that convert static grass to actual groundcover for a performance gain</li>
        <li>The final command-line tool many of our lists use is <a href="{% url 'mod_detail' slug='waza_lightfixes' %}">waza_lightfixes</a>, which tweaks lights in many ways making them more atmospheric (it's also configurable)</li>
        <li><a href="{% url 'mod_detail' slug='openmw-validator' %}">openmw-validator</a> is a tool that's optional, but it will help you catch mistakes in your <code>openmw.cfg</code> file</li>
		<li>The usage notes for these tools are all documented on each mod list's Final Steps page</li>
      </ul>
    </li>
  </ol>

  <p>Feel free to refer back to this list if you need a reminder about what to do, and please continue to read more about all of the above.</p>

  <h2>Table of contents</h2>

  <ul>
    <li><a href="#buy-morrowind">Setup Steps</a></li>
    <li><a href="#user-settings">Your User Settings</a></li>
    <li><a href="#choose-mod-list">Choose A Mod List</a></li>
	<li><a href="#folder-deploy-script">Folder Deploy Script</a></li>
    <li><a href="#reading-mod-detail">Reading A Mod Detail Page</a></li>
    <li><a href="#testing-your-progress">Testing Your Progress</a></li>
    <li><a href="#using-cfg-generator">Using The CFG Generator</a></li>
    <li><a href="#final-steps-verification">Final Steps & Verification</a></li>
    <li><a href="#further-reading">Further Reading</a></li>	
    <li><a href="#upcoming-changes">Upcoming Changes</a></li>
    <li><a href="#whats-next">What's Next?</a></li>
  </ul>

  <h3 id="buy-morrowind"><a href="#buy-morrowind">Setup Steps</a></h3>

  <p>
    One must of course <a href="{% url 'gs-buy' %}">buy a copy of Morrowind</a> and <a href="{% url 'gs-install' %}">install OpenMW</a>.
  </p>

  <blockquote>
    Note that Android is not officially supported by the OpenMW project or this website, and that version handles content management differently than every other version so a lot of what's talked about on this website may not apply.
  </blockquote>

  <p>
    Once you have Morrowind installed you will need to decide whether you want to install the latest stable version of OpenMW, or the latest development build. The development build may be in the early stages of development and may have bugs. If you decide to use the development build, however, you can opt-in to several mods that take advantage of the latest features in development. You can always find the list mods that require the development build using the <code>Tags</code> link on the bottom of every page on the site.
  </p>

  <p>
    <span class='bold'>OpenMW Stable installer instructions:</span>
    <ul>
      <li>For Windows, <a href='https://openmw.org/downloads'>download the installer</a> for the latest stable release</li>
      <li>Run the installer. It will have you choose the location where you would like to install OpenMW</li>
      <li>Open the OpenMW Launcher located in the folder where you installed OpenMW</li>
      <li>The first time you run the launcher it will start the Installation Wizard. If it doesn't, navigate to the <code>Settings</code> tab and run the Installation Wizard from there</li>
      <li>The installation wizard will have you find your vanilla Morrowind Data Files folder. Also, make sure you import all settings from the Morrowind.ini file when it gives you the option</li> 
	</ul>
	<span class='bold'>OpenMW Development Build installation instructions</span>
	<ul>
      <li><a href='https://openmw.org/downloads'>Download the latest development build</a></li>
      <li>Note: This requires the <code>Visual C++ redistributable</code> to be installed, and a link is provided underneath</li>
      <li>Extract the contents of the <code>.zip</code> archive to the location where you would like to install OpenMW</li>
      <li>Open the OpenMW Launcher located in the folder you extracted the archive</li>
      <li>The first time you run the launcher it will start the Installation Wizard. If it doesn't, navigate to the <code>Settings</code> tab and run the Installation Wizard from there</li>
      <li>The installation wizard will have you find your vanilla Morrowind Data Files folder. Also, make sure you import all settings from the Morrowind.ini file when it gives you the option</li>
	</ul>
  </p>

  <p>
    Once you've got OpenMW installed and are able to run vanilla Morrowind, you can also take a look at the various <a href="{% url 'gs-settings' %}">settings</a>. Some settings are available directly in the launcher and others must be added to your <code>settings.cfg</code> file. <span class="bold">It is vitally important that you DO NOT edit the <code>settings.cfg</code> file while the launcher is open, and also DO NOT open the launcher while the <code>settings.cfg</code> file is still open</span>.
  </p>

  <h3 id="user-settings"><a href="#user-settings">Your User Settings</a></h3>
  <p>
    Visit <a href="{% url 'user_settings' %}">the user settings page</a> and enter values for your Base Folder; that is where you want to install mods to; and your vanilla Morrowind Data Files folder. This allows the website to give you paths that reflect what will actually be on your computer, allowing for the copy-paste snippets provided by the CFG Generator (<a href="#using-cfg-generator">more on that below</a>).
  </p>

  <p>
    Below those fields there is a checkbox you can check if you want to opt-in to the <a href="/mods/tag/dev-build-only/">development build mods</a> which take advantage of OpenMW's latest features in development. Please be mindful that the development build and any development build mods may contain bugs or otherwise negatively impact your experience. By default, users are opted out of any development build mods.
  </p>
  
  <p>
    There are also options to toggle the website's built-in Dark and Light modes, or to clear your site data if the need arises.
  </p>

  <h3 id="choose-mod-list"><a href="#choose-mod-list">Choose A Mod List</a></h3>

  <p>
    <a href="{% url 'mod-lists' %}">Browse the catalogue of mod lists</a> and pick one that sounds good to you. Although many mods depend on something else to function properly, the majority of mods in most lists can be skipped or swapped for an alternate if desired. Please note that the larger mod lists do take quite a bit more time and effort to set up properly and are generally not recommended for first-time players of Morrowind. Check out Just Good Morrowind or I Heart Vanilla for a much faster and more vanilla-friendly experience.
  </p>

  <p>
    Below is a list of the main mod lists and their descriptions:
  </p>

  <ul>
    {% for list in mod_lists %}
      <li><a class="bold" href="{{ list.get_absolute_url }}">{{ list.title }} ({{ list.mod_count }} mods)</a>: {{ list.short_description|safe }}</li>
    {% endfor %}
  </ul>

  <h3 id="folder-deploy-script"><a href="#folder-deploy-script">Folder Deploy Script</a></h3>
  <p>
    After you choose a mod list, generate and run your folder deploy script using the instructions below. In order for this to work correctly you must have already set your <a href="#user-settings">User Settings</a>.
  </p>
  
  <p><span class="bold">Choose a mod list and your operating system and click Submit to generate your script:</span></p>
  <form id="folder-deploy" action="#the-openmw-way" method="get">
    <select name="modlist">
      <option {% if modlist == "i-heart-vanilla" %}selected{% endif %} value="i-heart-vanilla">I Heart Vanilla</option>
      <option {% if modlist == "i-heart-vanilla-directors-cut" %}selected{% endif %} value="i-heart-vanilla-directors-cut">I Heart Vanilla: DC</option>
      <option {% if modlist == "just-good-morrowind" %}selected{% endif %} value="just-good-morrowind">Just Good Morrowind</option>
      <option {% if modlist == "one-day-morrowind-modernization" %}selected{% endif %} value="one-day-morrowind-modernization">One Day Morrowind Modernization</option>
      <option {% if modlist == "graphics-overhaul" %}selected{% endif %} value="graphics-overhaul">Graphics Overhaul</option>
      <option {% if modlist == "expanded-vanilla" %}selected{% endif %} value="expanded-vanilla">Expanded Vanilla</option>
      <option {% if modlist == "total-overhaul" %}selected{% endif %} value="total-overhaul">Total Overhaul</option>
      <option {% if modlist == "starwind-modded" %}selected{% endif %} value="starwind-modded">Starwind Modded</option>
    </select>
    <select name="os">
      <option {% if os == "windows" %}selected{% endif %} value="windows">Windows</option>
      <option {% if os == "linux" %}selected{% endif %} value="linux">Linux</option>
      <option {% if os == "macos" %}selected{% endif %} value="macos">macOS</option>
    </select>
    <input type="submit" value="Submit" />
  </form>

  {% if paths %}
    <div id="file-ext" data-ext="{% if os == 'linux' or os == 'macos' %}sh{% else %}ps1{% endif %}"></div>
    <div id="file-modlist" data-modlist="{{ modlist }}"></div>
    <p class="bold" style="font-size: 1.5em;">The file should be downloaded!</p>
    <a class="bold" style="font-size: 1.5em;" id="dl-script"></a>
    <details style="margin-bottom: 50px;">
      <summary style="font-size: 1.5em;" class="bold">Click to expand and see the script:</summary>
      <pre><code id="data-paths">{% if os != "windows" %}#!/bin/sh
{% endif %}
{{ paths }}
echo All folder paths have been created!</code></pre>
    </details>
  {% endif %}

  <p><span class="bold">IMPORTANT!</span> Windows users will need to navigate to the directory they downloaded this script to and <code>Shift+Right-click</code> the folder to bring up the context menu. Click <code>Open Powershell window here</code> and type the following command, replacing the file name to reflect the script you downloaded:</p>
  <p><code>Powershell.exe -executionpolicy bypass -File .\momw-folder-generator-MODLIST-NAME-HERE.ps1</code></p>

  <p>Now that you have your folders deployed, installing mods is as simple as downloading them and extracting the contents into their designated directory.</p>

  <h3 id="reading-mod-detail"><a href="#reading-mod-detail">Reading A Mod Detail Page</a></h3>
  <p>
    Each mod detail page has two tables of information. The top table includes general information about the mod and the bottom table contains instructions for installation. <a href="{% url 'example-mod-page' %}">This example page</a> details what each field contains. Feel free to open this page on a separate browser tab in case you need a reminder. This example page is also easily accessible on each mod detail page itself if you need it later on.
  </p>

  <p>
    It's important at this point for users of mod lists to ensure they have set their <a href = "#user-settings">User Settings</a> set. Doing this will ensure that all the folder data paths on each of these mod detail pages will reflect your own. For your own convenience, these same folder data paths are provided by the <a href='#folder-deploy-script'>Folder Deploy Script</a>.
  </p>

  <p>
    These folder data paths are where you will extract the mod files into. Also, you'll see which plugins need to be enabled from each mod and any usage notes that contain further instructions you need to follow. Users following mod lists don't need to do anything with the plugins aside from ensuring they are located in their designated folder data path.
  </p>

  <p>
    Once you have installed the mod on your system be sure to hit the <code>Enable for CFG Generator</code> button in order to keep track of your progress and also to generate custom <a href="{% url 'cfg_generator3' %}">CFG Generator</a> output. This can be useful if you want to test your progress at any point during installation.
  </p>

  <h3 id="testing-your-progress"><a href="#testing-your-progress">Testing Your Progress</a></h3>
  
  <p>
    It is important to keep in mind that some mods require assets from other mods you install later on, so it is expected for minor issues to occur before the list has been completed, but testing along the way is still a good way to make sure mods are enabled and that there are no errors in your operating procedure.
  </p>
  <p>
    <ul>
      <li>On the <a href="{% url 'cfg_generator' %}">CFG Generator</a> page, click the button labeled with the mod list you've chosen under where it says <code>Choose a preset</code></li>
      <li>On the resulting page, there will be a link labeled <code>Custom CFG Section</code></li>
      <li>There will be a list of mods with checkboxes, each of the ones you've clicked <code>Enable For CFG Generator</code> on will be checked</li>
      <li>You may check boxes here to enable them in the same way as clicking <code>Enable For CFG Generator</code> on a mod detail page</li>
      <li>At the bottom of this section there will be a button labeled <code>Submit Custom Setup</code>, click it</li>
      <li>The resulting page will present you with an adjusted version of the preset you selected, containing only the mods you've enabled thus far</li>
      <li>Near the top of this page there will be a section labeled <code>openmw.cfg Section</code>, followed by one labeled <code>settings.cfg Section</code></li>
      <li>Click the <code>Copy to clipboard</code> link at the top of each respective section to copy the entire snippet to your clipboard</li>
      <li>Paste each snippet into their respective file. This procedure is detailed in the <a href="#using-cfg-generator">next section</a></li>
      <li>For the <code>openmw.cfg</code> snippet, you'll want to overwrite the existing <code>data=</code> and <code>content=</code> lines placed there by OpenMW</li>
      <li>For the <code>settings.cfg</code> snippet, you will have to overwrite any existing sections or values you may already have. If the header for a section already exists in your file (ie: <code>[Shadows]</code>), make sure to omit it from the output of the CFG Generator and cut-and-paste those corresponding lines under the section that is already present instead. You may find it useful to simply rename any existing file you have to <code>settings.cfg.bak</code> and start with a fresh file, then edit to suit your liking from there</li>
    </ul>
  <p>
    Before testing, <a href="#using-cfg-generator">use the CFG Generator</a> to apply the correct load order to your <code>openmw.cfg</code>, <span class="bold">you will need to do this every time you test</span> as the launcher may disable entries that, while currently invalid, will not be invalid on subsequent tests. You will need to remove (or comment out with <code>#</code>) <code>groundcover</code> entries until you have installed them, as they will cause OpenMW to crash if they are not installed.
  </p>

  <h3 id="using-cfg-generator"><a href="#using-cfg-generator">Using The CFG Generator</a></h3>
  
  <p>
    The <a href="{% url 'cfg_generator3' %}">CFG Generator</a> will provide you with personalized lines for your mod list that you will need to copy-and-paste into your <code>openmw.cfg</code> and <code>settings.cfg</code> files in order for the mod list to function properly. <span class="bold">It is vitally important that you DO NOT edit the <code>openmw.cfg</code> or the <code>settings.cfg</code> file while the launcher is open, and also DO NOT open the launcher while those files are still open</span>.
  </p>
  <ul>
    <li>On Windows, these files are located in <code>Documents\My Games\OpenMW</code></li>
    <li>On Linux, these files are located in <code>~/.config/openmw</code></li>
    <li>On Mac, these files are located in <code>~/Library/Preferences/openmw</code></li>
  </ul>
  
  <ul>
    <li>Ensure that your <a href="/settings">User Settings</a> are set. With your User Settings set click on the <code>Choose a preset</code> button corresponding to the mod list you chose. After a couple of seconds you will have your config files generated for you.</li>
    <li>The sections you will copy-and-paste into your config files can be quite large, and so for your convenience you can click on the <code>Copy to clipboard</code> button.</li>
    <li>For the <code>openmw.cfg</code> snippet, copy-and-paste it below the last line starting with <code>fallback=</code>. Replace everything after this line, including the <code>data=</code> and <code>content=</code> lines already present, and verify the <code>data=</code> folder paths match your own files. If there is a mismatch here you may have set your <a href="/settings">User Settings</a> incorrectly or deviated from the folder naming schemes provided by this website.</li>
    <li>For the <code>settings.cfg</code> snippet, you will have to overwrite any existing sections or values you may already have. If the header for a section already exists in your file (ie: <code>[Shadows]</code>), make sure to omit it from the output of the CFG Generator and cut-and-paste those corresponding lines under the section that is already present instead. You may find it useful to simply rename any existing file you have to <code>settings.cfg.bak</code> and start with a fresh file, then edit to suit your liking from there</li>
  </ul>
  <p>
    Note that the CFG Generator creates a complete list of plugins that you should have enabled once you have gone through a mod list and finished its Final Steps. Once you finish copy-and-pasting you may have <code>data=</code>, <code>content=</code>, and <code>groundcover=</code> lines for mods you've not installed yet. If you haven't installed those mods yet, it is important that you remove those lines or comment them out by placing a <code>#</code> at the start of the line until you do install those mods.
  </p>

  <h3 id="final-steps-verification"><a href="#final-steps-verification">Final Steps & Verification</a></h3>

  <p>
    Each mod list has a "Final Steps" page (<a href="{% url 'mod-list-final' slug='expanded-vanilla' %}">Expanded Vanilla's final steps page</a>, as an example) that has you use <a href="#using-cfg-generator">the CFG Generator</a> and any tools required by the mod list to function correctly. You will also validate your files to ensure you've installed everything correctly and clean any plugins that require it. 
  </p>

  <h3 id="further-reading"><a href="#further-reading">Further Reading</a></h3>

  <p>
    A few pages that you'll want to reference while on your modding journey:
  </p>

  <ul>
    <li>The <a href="{% url 'tips-performance' %}">Performance Tips</a> page gives an overview of the various options for adjusting visual quality and performance</li>
    <li>The <a href="{% url 'tips-navmeshtool' %}">Navmeshtool</a> page details how to generate a Nav Mesh to improve cell loading performance</li>
    {# <li>The <a href="{% url 'tips-register-bsas' %}">Registering BSAs</a> page is also referenced from the <a href="{% url 'gs-directories' %}">Managing Mods</a> page, check it out</li> #}
  </ul>

  <h3 id="upcoming-changes"><a href="#upcoming-changes">Upcoming Changes</a></h3>

  <p>
    Wondering if there's going to be an update soon? Want to know what's being worked on with this website? All changes to the website go out to <a href="https://beta.modding-openmw.com/">the beta site</a> first, but you can also refer to <a href="https://gitlab.com/modding-openmw/modding-openmw.com/activity">the GitLab activity log</a> to see what's been going on with the project.
  </p>

  <h3 id="whats-next"><a href="#whats-next">What's Next?</a></h3>

  <p>
    From here, if you've got further interest in the community - or simply need to report a problem or ask questions - come hang out in the project <a href="https://discord.gg/KYKEUxFUsZ">Discord</a> or <a href="https://web.libera.chat/?channels=#momw">IRC</a> channels and discuss all things related to this project, OpenMW, and Morrowind modding in general.
  </p>

  <p>
    Please also feel free to check <a href="https://www.twitch.tv/johnnyhostile">the project Twitch stream</a>. The website creator streams twice a week and works on improving and updating the website.
  </p>

  <p>
    There's also <a href="https://gitlab.com/modding-openmw/modding-openmw.com/-/issues">the project issue tracker on GitLab</a> where you can view existing issues or file a new one if you find something that needs to be fixed or updated.
  </p>

  <p>
    This website's mod lists and content receive regular updates, including each week on the regular project stream, so be sure to check <a href="{% url 'changelogs' %}">the mod list</a> and <a href="{% url 'changelogs-website' %}">website changelogs</a> often. And happy modding!
  </p>

  {% include "main-nav.html" with cur_page="guides_users" %}

{% endblock %}
