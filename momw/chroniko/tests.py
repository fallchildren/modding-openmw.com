from django.contrib.auth.models import AnonymousUser, User
from django.core.exceptions import PermissionDenied
from django.http.response import Http404
from django.shortcuts import reverse
from django.test import RequestFactory, TestCase
from django.utils import timezone
from chroniko.data_seeds.blog import generate_category, generate_entry
from chroniko.models import BlogEntry
from .models import BlogCategory, BlogTag
from .views import (
    CategoryDetailView,
    CategoryIndexView,
    DraftEntriesView,
    EntryDetailView,
    EntryIndexView,
    HiddenEntriesView,
    TagCloudView,
    TagDetailView,
    category_create,
    category_edit,
    entry_create,
    entry_delete,
    entry_edit,
    tag_edit,
)


HTTP_FOUND = 302
HTTP_PERMISSION_DENIED = 403
HTTP_NOT_FOUND = 404
HTTP_OK = 200


class SetUpBase:
    def setUp(self):
        super().setUp()
        self.f = RequestFactory()
        # TODO: don't create a user here
        if not User.objects.all():
            self.u = User.objects.create(
                first_name="First Name",
                last_name="Last Name",
                username="first-last-jaja-lala",
                password="12345",
                is_superuser=True,
            )
        self.c = generate_category("Category One", "one", "This is category one, yeah!")
        self.c2 = generate_category(
            "Category Two", "TWOOOOOOSWEEEET", "This is category two, BoiiII!"
        )
        self.e = generate_entry(
            "BODY " * 300,
            timezone.localtime(timezone.now()),
            "test-slug",
            BlogEntry.LIVE,
            "Test Title",
            self.u,
            self.c,
        )
        self.e2 = generate_entry(
            "BODY " * 300,
            timezone.localtime(timezone.now()),
            "test-slug2",
            BlogEntry.LIVE,
            "Test Title 2",
            self.u,
            self.c2,
            excerpt="EXCERPT " * 300,
        )
        self.t = BlogTag.objects.create(name="live")
        # TODO: use generate_entry() ??
        self.el = BlogEntry.objects.create(
            excerpt="EXCERPT TIME",
            body="BODY TIME",
            date_added=timezone.now(),
            slug="live-1",
            status=BlogEntry.LIVE,
            title="Live ONE",
            author=self.u,
            category=self.c,
        )
        self.ed = BlogEntry.objects.create(
            excerpt="EXCERPT TIME",
            body="BODY TIME",
            date_added=timezone.now(),
            slug="draft-1",
            status=BlogEntry.DRAFT,
            title="Draft ONE",
            author=self.u,
            category=self.c,
        )
        self.eh = BlogEntry.objects.create(
            excerpt="EXCERPT TIME",
            body="BODY TIME",
            date_added=timezone.now(),
            slug="hidden-1",
            status=BlogEntry.HIDDEN,
            title="Hidden ONE",
            author=self.u,
            category=self.c,
        )


class ChronikoSeleniumTestCase(SetUpBase):
    """
    To use this class, subclass it and the BaseSelenium class from 'utilz' as
    seen below.  Ordering matters:

    class YourFirefoxTestCase(BaseSelenium, YourSeleniumTestCase,
                              ChronikoSeleniumTestCase, StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.driver = FFDriver()

    Where 'YourSeleniumTestCase' is a class that implements Selenium test methods.
    """

    def test_view_entry_index(self):
        u = "blog:index"
        v = EntryIndexView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        self.check_for_text(u, self.e.category.title)
        self.check_for_text(u, self.e2.category.title)
        # TODO: The below two tests are sometimes flaky...
        self.check_for_text(u, self.e.body[:25])
        self.check_for_text(u, self.e2.excerpt[:25])
        self.check_for_text(u, self.e.title)
        self.check_for_text(u, self.e2.title)
        self.check_for_text(u, self.e.date_added.strftime("%Y-%m-%d"))
        self.check_for_text(u, self.e2.date_added.strftime("%Y-%m-%d"))

    def test_view_category_index(self):
        u = "blog:category_index"
        v = CategoryIndexView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        self.check_for_text(u, self.c.title)
        self.check_for_text(u, self.c2.title)

    def test_view_new_category_and_cant(self):
        u = "blog:new_category"
        v = category_create
        with self.assertRaises(Http404):
            self.check_http_status_code(u, v)
        self.check_for_500(u)

    def test_view_category_detail(self):
        u = "blog:category_detail"
        v = CategoryDetailView.as_view()
        self.check_http_status_code(u, v, slug=self.c.slug)
        self.check_for_500(u, slug=self.c.slug)
        self.check_for_text(u, self.c.title, slug=self.c.slug)

    # TODO: this view isn't written yet!
    # def test_view_category_delete_and_cant(self):
    #     u = "blog:category_delete"
    #     v = category_delete
    #     with self.assertRaises(Http404):
    #         self.check_http_status_code(u, v, slug=self.c.slug)
    #     self.check_for_500(u, slug=self.c.slug)

    def test_view_category_edit_and_cant(self):
        u = "blog:category_edit"
        v = category_edit
        with self.assertRaises(Http404):
            self.check_http_status_code(u, v, slug=self.c.slug)
        self.check_for_500(u, slug=self.c.slug)

    def test_view_drafts_and_cant(self):
        u = "blog:draft_entries"
        v = DraftEntriesView.as_view()
        with self.assertRaises(PermissionDenied):
            self.check_http_status_code(u, v)
        self.check_for_500(u)

    # TODO: this fails because the view is legit broken!
    # def test_view_all_entries(self):
    #     u = "blog:all_entries"
    #     v = AllEntriesView.as_view()
    #     self.check_http_status_code(u, v)
    #     self.check_for_500(u)
    #     self.check_for_text(u, self.e.category.title)
    #     self.check_for_text(u, self.e2.category.title)
    #     # TODO: The below two tests are sometimes flaky...
    #     self.check_for_text(u, self.e.body[:25])
    #     self.check_for_text(u, self.e2.excerpt[:25])
    #     self.check_for_text(u, self.e.title)
    #     self.check_for_text(u, self.e2.title)
    #     self.check_for_text(u, self.e.date_added.strftime("%Y-%m-%d"))
    #     self.check_for_text(u, self.e2.date_added.strftime("%Y-%m-%d"))

    def test_view_new_entry_and_cant(self):
        u = "blog:new_entry"
        v = entry_create
        with self.assertRaises(Http404):
            self.check_http_status_code(u, v)
        self.check_for_500(u)

    # TODO: this fails, dunno why...
    # def test_view_latest_entries_feed(self):
    #     u = "blog:latest_entries_feed"
    #     v = LatestEntriesFeed()
    #     self.check_http_status_code(u, v)
    #     self.check_for_500(u)

    def test_view_hidden_and_cant(self):
        u = "blog:hidden_entries"
        v = HiddenEntriesView.as_view()
        with self.assertRaises(PermissionDenied):
            self.check_http_status_code(u, v)
        self.check_for_500(u)

    def test_view_tag_cloud(self):
        u = "blog:tag_cloud"
        v = TagCloudView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        self.check_for_text(u, self.t.name)

    def test_view_tag_detail(self):
        u = "blog:tag_detail"
        v = TagDetailView.as_view()
        self.check_http_status_code(u, v, slug=self.t.slug)
        self.check_for_500(u, slug=self.t.slug)
        self.check_for_text(u, self.t.name, slug=self.t.slug)

    def test_view_tag_edit_and_cant(self):
        u = "blog:tag_edit"
        v = tag_edit
        with self.assertRaises(Http404):
            self.check_http_status_code(u, v, slug=self.t.slug)
        self.check_for_500(u, slug=self.t.slug)

    def test_view_entry_delete_and_cant(self):
        u = "blog:entry_delete"
        v = entry_delete
        with self.assertRaises(Http404):
            self.check_http_status_code(u, v, slug=self.e.slug)
        self.check_for_500(u, slug=self.e.slug)

    def test_view_entry_edit_and_cant(self):
        u = "blog:entry_edit"
        v = entry_edit
        with self.assertRaises(Http404):
            self.check_http_status_code(u, v, slug=self.e.slug)
        self.check_for_500(u, slug=self.e.slug)

    # TODO: generic class-based view functions are, of course, a total bitch to test and troubleshoot...
    # def test_view_by_year(self):
    #     u = "blog:by_year"
    #     v = EntriesByYearView.as_view()
    #     # self.check_http_status_code(u, v)
    #     self.check_http_status_code(u, v, year=self.e.date_added.strftime("%Y"))
    #     # self.check_for_500(u, year=self.e.date_added.strftime("%Y"))
    #     # self.check_for_text(u, self.e.title, year=self.e.date_added.strftime("%Y"))


# TODO: many if not all of the below are obsoleted by the above selenium tests
class ChronikoTestCase(SetUpBase, TestCase):
    def test_cat_absolute_url(self):
        self.assertIsInstance(self.c.get_absolute_url(), str)

    def test_entry_absolute_url(self):
        self.assertIsInstance(self.el.get_absolute_url(), str)

    def test_tag_absolute_url(self):
        self.assertIsInstance(self.t.get_absolute_url(), str)

    # # TODO: need test for timezone
    def test_live_entry_is_viewable(self):
        year = self.el.date_added.year
        month = self.el.date_added.month
        day = self.el.date_added.day
        slug = self.el.slug
        r = self.f.get(self.el.get_absolute_url())
        r.user = AnonymousUser()
        response = EntryDetailView.as_view()(
            r, year=year, month=month, day=day, slug=slug
        )
        self.assertEqual(response.status_code, HTTP_OK)

    def test_live_entry_is_viewable_short_url(self):
        slug = self.el.slug
        r = self.f.get(self.el.get_absolute_url())
        r.user = AnonymousUser()
        resonse = EntryDetailView.as_view()(r, slug=slug)
        self.assertEqual(resonse.status_code, HTTP_OK)

    def test_entry_index(self):
        r = self.f.get(reverse("blog:index"))
        r.user = AnonymousUser()
        response = EntryIndexView.as_view()(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_category_index(self):
        r = self.f.get(reverse("blog:category_index"))
        r.user = AnonymousUser()
        response = CategoryIndexView.as_view()(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_category_detail(self):
        r = self.f.get(reverse("blog:category_detail", kwargs={"slug": self.c.slug}))
        r.user = AnonymousUser()
        response = CategoryDetailView.as_view()(r, slug=self.c.slug)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_drafts_index_is_not_anonymously_viewable(self):
        r = self.client.get(reverse("blog:draft_entries"))
        r.user = AnonymousUser()
        self.assertEqual(r.status_code, HTTP_PERMISSION_DENIED)

    def test_hidden_index_is_not_anonymously_viewable(self):
        r = self.client.get(reverse("blog:hidden_entries"))
        r.user = AnonymousUser()
        self.assertEqual(r.status_code, HTTP_PERMISSION_DENIED)

    def test_category_create_is_not_anonymously_viewable(self):
        r = self.client.get(reverse("blog:new_category"))
        r.user = AnonymousUser()
        self.assertEqual(r.status_code, HTTP_NOT_FOUND)

    def test_category_create_post(self):
        r = self.f.post(
            reverse("blog:new_category"),
            data={"title": "New 1", "slug": "new-1", "description": "STUFF HERE"},
        )
        r.user = self.u
        category_create(r)
        self.assertTrue(BlogCategory.objects.get(slug="new-1"))

    def test_category_edit_post(self):
        c = self.c
        r = self.f.post(
            reverse("blog:category_edit", kwargs={"slug": c.slug}),
            data={"title": "New 11", "slug": "new-11", "description": "NEW STUFF HERE"},
        )
        r.user = self.u
        category_edit(r, slug=c.slug)
        c_edited = BlogCategory.objects.get(pk=c.pk)
        self.assertTrue(c_edited.slug == "new-11")

    # def test_category_delete(self):
    #     pass

    def test_entry_create_is_not_anonymously_viewable(self):
        r = self.client.get(reverse("blog:new_entry"))
        r.user = AnonymousUser()
        self.assertEqual(r.status_code, HTTP_NOT_FOUND)
