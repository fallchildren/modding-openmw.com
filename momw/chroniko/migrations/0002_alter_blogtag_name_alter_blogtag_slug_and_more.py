# Generated by Django 4.2.7 on 2023-11-05 02:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("contenttypes", "0002_remove_content_type_name"),
        ("chroniko", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="blogtag",
            name="name",
            field=models.CharField(max_length=100, unique=True, verbose_name="name"),
        ),
        migrations.AlterField(
            model_name="blogtag",
            name="slug",
            field=models.SlugField(
                allow_unicode=True, max_length=100, unique=True, verbose_name="slug"
            ),
        ),
        migrations.AlterField(
            model_name="taggedentry",
            name="content_type",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="%(app_label)s_%(class)s_tagged_items",
                to="contenttypes.contenttype",
                verbose_name="content type",
            ),
        ),
        migrations.AlterField(
            model_name="taggedentry",
            name="object_id",
            field=models.IntegerField(db_index=True, verbose_name="object ID"),
        ),
        migrations.AlterField(
            model_name="taggedentry",
            name="tag",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="%(app_label)s_%(class)s_items",
                to="chroniko.blogtag",
            ),
        ),
    ]
