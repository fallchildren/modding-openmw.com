from django import forms
from .models import BlogCategory, BlogEntry, BlogTag


class CategoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        fields = "__all__"
        model = BlogCategory


class TagForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        fields = "__all__"
        model = BlogTag


class DeleteThingForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["delete_or_not"].label = "I confirm that I want to delete this!"

    delete_or_not = forms.BooleanField()


class EntryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["category"].widget.attrs["class"] = "u-full-width"
        self.fields["status"].widget.attrs["class"] = "u-full-width"
        self.fields["tags"].label = "Comma-separated list of tags"

    class Meta:
        exclude = ("author", "date_added")
        model = BlogEntry

    date_added_0 = forms.DateField(label="Publish Date", required=True)
    date_added_1 = forms.TimeField(label="Publish Time", required=True)


class ToggleTinyMCEForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    toggle_tinymce = forms.BooleanField(
        help_text="Toggle the TinyMCE editor on or off.  Any unsaved data will be lost!!"
    )
