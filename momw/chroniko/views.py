from datetime import datetime
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, reverse, get_object_or_404
from django.views.generic import (
    DayArchiveView,
    DetailView,
    ListView,
    MonthArchiveView,
    YearArchiveView,
)
from utilz.cache import CacheCtl
from . import CHANGE_CATEGORY_PERM, CHANGE_ENTRY_PERM, ENTRY_DATE_FIELD
from .forms import CategoryForm, DeleteThingForm, EntryForm, TagForm, ToggleTinyMCEForm
from .models import BlogCategory, BlogEntry, BlogTag, TaggedEntry


# TODO: use a mixin for pagination
class AllEntriesView(CacheCtl, ListView):
    context_object_name = "all_entries"
    date_field = ENTRY_DATE_FIELD
    queryset = BlogEntry.live.all()
    template_name = "all_entries.html"
    paginate_by = settings.PAGINATE_BY


class DraftEntriesView(LoginRequiredMixin, ListView):
    context_object_name = "draft_entries"
    date_field = ENTRY_DATE_FIELD
    queryset = BlogEntry.draft.all()
    raise_exception = True
    template_name = "draft_entries.html"
    paginate_by = settings.PAGINATE_BY


class EntriesByDayView(CacheCtl, DayArchiveView):
    context_object_name = "entries_by_day"
    date_field = ENTRY_DATE_FIELD
    make_object_list = True
    month_format = settings.MONTH_FORMAT
    paginate_by = settings.PAGINATE_BY
    queryset = BlogEntry.live.all()
    template_name = "entries_by_day.html"
    paginate_by = settings.PAGINATE_BY


class EntriesByMonthView(CacheCtl, MonthArchiveView):
    context_object_name = "entries_by_month"
    date_field = ENTRY_DATE_FIELD
    make_object_list = True
    month_format = settings.MONTH_FORMAT
    paginate_by = settings.PAGINATE_BY
    queryset = BlogEntry.live.all()
    template_name = "entries_by_month.html"
    paginate_by = settings.PAGINATE_BY


class EntriesByYearView(CacheCtl, YearArchiveView):
    context_object_name = "entries_by_year"
    date_field = ENTRY_DATE_FIELD
    make_object_list = True
    month_format = settings.MONTH_FORMAT
    paginate_by = settings.PAGINATE_BY
    queryset = BlogEntry.live.all()
    template_name = "entries_by_year.html"
    paginate_by = settings.PAGINATE_BY


class EntryDetailView(CacheCtl, DetailView):
    context_object_name = "entry"
    model = BlogEntry
    month_format = settings.MONTH_FORMAT
    queryset = BlogEntry.live.all()
    template_name = "entry_detail.html"
    paginate_by = settings.PAGINATE_BY


class EntryIndexView(CacheCtl, ListView):
    context_object_name = "entry_list"
    date_field = ENTRY_DATE_FIELD
    paginate_by = settings.PAGINATE_BY
    paginate_orphans = 4
    queryset = BlogEntry.live.all()
    template_name = "entry_index.html"
    paginate_by = settings.PAGINATE_BY


class HiddenEntriesView(LoginRequiredMixin, ListView):
    context_object_name = "hidden_entries"
    date_field = ENTRY_DATE_FIELD
    queryset = BlogEntry.hidden.all()
    raise_exception = True
    template_name = "hidden_entries.html"
    paginate_by = settings.PAGINATE_BY


class CategoryDetailView(CacheCtl, DetailView):
    context_object_name = "category"
    model = BlogCategory
    template_name = "category_detail.html"
    paginate_by = settings.PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        context["category_live_entries"] = BlogEntry.live.filter(
            category=context["object"].pk
        )
        return context


class CategoryIndexView(CacheCtl, ListView):
    context_object_name = "categories"
    model = BlogCategory
    template_name = "category_index.html"
    paginate_by = settings.PAGINATE_BY


class TagCloudView(CacheCtl, ListView):
    context_object_name = "tags"
    queryset = BlogTag.enabled.all()
    template_name = "tag_cloud.html"
    paginate_by = settings.PAGINATE_BY


class TagDetailView(CacheCtl, DetailView):
    context_object_name = "tag"
    model = BlogTag
    template_name = "tag_detail.html"
    paginate_by = settings.PAGINATE_BY


def category_create(request):
    form = CategoryForm()
    user = request.user
    if user.is_authenticated and user.has_perm(CHANGE_CATEGORY_PERM):
        if request.method == "POST":
            postdata = request.POST.copy()
            form = CategoryForm(postdata)
            if form.is_valid():
                form.save()
                # Redirect to the Category's edit page,
                # in case the slug was changed.
                return HttpResponseRedirect(
                    reverse("blog:category_edit", kwargs={"slug": postdata.get("slug")})
                )
        return render(request, "category_create.html", {"form": form})
    else:
        raise Http404


def _delete_category():
    pass


def category_delete():
    pass


def category_edit(request, slug):
    category = get_object_or_404(BlogCategory, slug=slug)
    form = CategoryForm()
    user = request.user
    if user.is_authenticated and user.has_perm(CHANGE_CATEGORY_PERM):
        if request.method == "POST":
            postdata = request.POST.copy()
            form = CategoryForm(postdata, instance=category)
            if form.is_valid():
                form.save()
                # Redirect to the Category's edit page,
                # in case the slug was changed.
                return HttpResponseRedirect(
                    reverse("blog:category_edit", kwargs={"slug": postdata.get("slug")})
                )
        return render(
            request, "category_edit.html", {"category": category, "form": form}
        )
    else:
        raise Http404


def entry_create(request):
    """Create a new Entry."""
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    user = request.user
    try:
        toggle_tinymce = settings.USE_TINYMCE
    except AttributeError:
        toggle_tinymce = False
    if user.is_authenticated and user.has_perm(CHANGE_ENTRY_PERM):
        form = EntryForm()
        toggleform = ToggleTinyMCEForm()
        if request.method == "POST":
            postdata = request.POST.copy()
            if "toggleTinyMCE" not in postdata:
                form = EntryForm(postdata)
                if form.is_valid():
                    entry = form.instance
                    entry.author = user
                    # Update the date and time if need be.
                    new_date = postdata.get("date_added_0", None)
                    new_time = postdata.get("date_added_1", None)
                    if new_date and new_time:
                        new_datetime_string = " ".join(
                            (
                                new_date,
                                "".join(
                                    (
                                        new_time,
                                        " ".join((":00", settings.TIME_ZONE_OFFSET)),
                                    )
                                ),
                            )
                        )
                        new_datetime = datetime.strptime(
                            new_datetime_string, datetime_format
                        )
                        entry.date_added = new_datetime
                    form.save()
                    # Update the Entry's tags if need be.
                    new_tags = postdata.get("tags", None)
                    if new_tags:
                        for t in new_tags.split(", "):
                            entry.tags.add(t)
                            tag = BlogTag.objects.get(name=t)
                            entry_content_type = ContentType.objects.get_for_model(
                                BlogEntry
                            )
                            TaggedEntry(
                                content_type_id=entry_content_type.id,
                                object_id=entry.id,
                                tag=tag,
                            ).save()
                    # Redirect to the Entry's edit page,
                    # in case the slug was changed.
                    return HttpResponseRedirect(
                        reverse(
                            "slug_entry_edit", kwargs={"slug": postdata.get("slug")}
                        )
                    )
            elif "toggleTinyMCE" in postdata:
                form = EntryForm()
                toggleform = ToggleTinyMCEForm(postdata)
                if toggleform.is_valid():
                    if postdata.get("toggle_tinymce", None) == "on":
                        toggle_tinymce = True
        else:  # if request.method == 'POST':
            postdata = None
            toggleform = ToggleTinyMCEForm()
        return render(
            request,
            "entry_create.html",
            {"form": form, "toggle_tinymce": toggle_tinymce, "toggleform": toggleform},
        )
    else:  # if user.is_authenticated and user.has_perm(CHANGE_ENTRY_PERM):
        # Anonymous user
        raise Http404


def tag_edit(request, slug):
    tag = get_object_or_404(BlogTag, slug=slug)
    form = TagForm()
    user = request.user
    if user.is_authenticated and user.has_perm(CHANGE_CATEGORY_PERM):
        if request.method == "POST":
            postdata = request.POST.copy()
            form = TagForm(postdata, instance=tag)
            if form.is_valid():
                form.save()
                # Redirect to the Category's edit page,
                # in case the slug was changed.
                return HttpResponseRedirect(
                    reverse("blog:tag_edit", kwargs={"slug": postdata.get("slug")})
                )
        return render(request, "tag_edit.html", {"tag": tag, "form": form})
    else:
        raise Http404


def _delete_entry(entry):
    try:
        entry.delete()
        return True
    except AssertionError:
        # Entry was probably already deleted...
        return False


def entry_delete(request, slug):
    deleted = False
    deleted_name = None
    user = request.user
    if user.is_authenticated and user.has_perm(CHANGE_ENTRY_PERM):
        entry = get_object_or_404(BlogEntry, slug=slug)
        form = DeleteThingForm()
        if request.method == "POST":
            postdata = request.POST.copy()
            form = DeleteThingForm(postdata)
            if form.is_valid():
                deleted_name = entry.__str__()
                deleted = _delete_entry(entry)
                return render(
                    request,
                    "entry_delete.html",
                    {
                        "deleted": deleted,
                        "deleted_name": deleted_name,
                        "entry": entry,
                        "form": form,
                    },
                )
        return render(
            request,
            "entry_delete.html",
            {
                "deleted": deleted,
                "deleted_name": deleted_name,
                "entry": entry,
                "form": form,
            },
        )
    else:  # if user.is_authenticated and user.has_perm(CHANGE_ENTRY_PERM):
        # Anonymous user
        raise Http404


def entry_edit(request, slug):
    """Edit the Entry with the slug 'slug'."""
    date_string = "%Y-%m-%d"
    time_string = "%H:%M"
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    entry = get_object_or_404(BlogEntry, slug=slug)
    entry_date_added = entry.date_added.astimezone().date().strftime(date_string)
    entry_time_added = entry.date_added.astimezone().time().strftime(time_string)
    user = request.user
    entry_tag_list = []
    try:
        toggle_tinymce = settings.USE_TINYMCE
    except AttributeError:
        toggle_tinymce = False
    [entry_tag_list.append(t.name) for t in entry.tags.all()]
    if user.is_authenticated and user.has_perm(CHANGE_ENTRY_PERM):
        toggleform = ToggleTinyMCEForm()
        if request.method == "POST":
            postdata = request.POST.copy()
            # We are trying to post changes to an Entry.
            if "toggleTinyMCE" not in postdata:
                form = EntryForm(postdata, instance=entry)
                if form.is_valid():
                    form.save()
                    # Update the date and time if need be.
                    new_date = postdata.get("date_added_0", None)
                    new_time = postdata.get("date_added_1", None)
                    if new_date and new_time:
                        new_datetime_string = " ".join(
                            (
                                new_date,
                                "".join(
                                    (
                                        new_time,
                                        " ".join((":00", settings.TIME_ZONE_OFFSET)),
                                    )
                                ),
                            )
                        )
                        new_datetime = datetime.strptime(
                            new_datetime_string, datetime_format
                        )
                        entry.date_added = new_datetime
                        entry.save()
                    # Update the Entry's tags if need be.
                    for t in entry_tag_list:
                        if len(t) > 0:
                            if t not in entry_tag_list:
                                entry.tags.add(t)
                                tag = BlogTag.objects.get(name=t)
                                entry_content_type = ContentType.objects.get_for_model(
                                    BlogEntry
                                )
                                TaggedEntry(
                                    content_type_id=entry_content_type.id,
                                    object_id=entry.id,
                                    tag=tag,
                                ).save()
                    # Redirect to the Entry's edit page,
                    # in case the slug was changed.
                    # TODO: pass along some sort of message when this is done
                    return HttpResponseRedirect(
                        reverse(
                            "slug_entry_edit", kwargs={"slug": postdata.get("slug")}
                        )
                    )
            elif "toggleTinyMCE" in postdata:
                form = EntryForm(instance=entry)
                toggleform = ToggleTinyMCEForm(postdata)
                if toggleform.is_valid():
                    if postdata.get("toggle_tinymce", None) == "on":
                        toggle_tinymce = True
        else:  # if request.method == 'POST':
            # We are just looking at an Entry
            form = EntryForm(instance=entry)
            postdata = None
            toggleform = toggleform = ToggleTinyMCEForm()
        return render(
            request,
            "entry_edit.html",
            {
                "entry": entry,
                "entry_date_added": entry_date_added,
                "entry_time_added": entry_time_added,
                "entry_tag_list": ", ".join(entry_tag_list),
                "form": form,
                "postdata": postdata,
                "toggle_tinymce": toggle_tinymce,
                "toggleform": toggleform,
            },
        )
    else:  # if user.is_authenticated and user.has_perm(CHANGE_ENTRY_PERM):
        # Anonymous user
        raise Http404
