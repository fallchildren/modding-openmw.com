# Some constants for the blog
# TODO: de-hard code the app name
ADD_CATEGORY_PERM = "chroniko.add_blogcategory"
ADD_ENTRY_PERM = "chroniko.add_blogentry"
CHANGE_CATEGORY_PERM = "chroniko.change_blogcategory"
CHANGE_ENTRY_PERM = "chroniko.change_blogentry"
ENTRY_DATE_FIELD = "date_added"

default_app_config = "chroniko.apps.ChronikoConfig"
