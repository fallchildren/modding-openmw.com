import pytz

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.urls import reverse
from .models import BlogEntry


TZ = pytz.timezone(settings.TIME_ZONE)
try:
    DESCRIPTION = settings.BLOG_RSS_DESCRIPTION
    TITLE = settings.BLOG_RSS_TITLE
except AttributeError:
    TITLE = "{}'s Latest Entries".format(settings.SITE_NAME)
    DESCRIPTION = TITLE


class LatestEntriesFeed(Feed):
    description = DESCRIPTION
    link = "/blog/"  # TODO: find out wtf this is for
    title = TITLE

    def items(self):
        return BlogEntry.live.all()

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        if item.excerpt:
            return item.excerpt
        else:
            excerpt = item.body.split()[:50]
            return " ".join(excerpt)

    def item_link(self, item):
        return reverse(
            "blog:entry_detail",
            kwargs={
                "year": item.date_added.astimezone(TZ).strftime("%Y"),
                "month": item.date_added.astimezone(TZ).strftime("%m"),
                "day": item.date_added.astimezone(TZ).strftime("%d"),
                "slug": item.slug,
            },
        )
