from django.conf import settings
from django.urls import path
from django.views.decorators.cache import cache_page
from .feeds import LatestEntriesFeed
from .views import (
    AllEntriesView,
    CategoryDetailView,
    CategoryIndexView,
    DraftEntriesView,
    EntriesByDayView,
    EntriesByMonthView,
    EntriesByYearView,
    EntryDetailView,
    EntryIndexView,
    HiddenEntriesView,
    TagCloudView,
    TagDetailView,
    category_create,
    category_delete,
    category_edit,
    entry_create,
    entry_delete,
    entry_edit,
    tag_edit,
)


app_name = "chroniko"  # TODO: DRY this
urlpatterns = [
    path("", EntryIndexView.as_view(), name="index"),
    path("categories/", CategoryIndexView.as_view(), name="category_index"),
    path("categories/new/", category_create, name="new_category"),
    path(
        "categories/<slug:slug>/", CategoryDetailView.as_view(), name="category_detail"
    ),
    path("categories/<slug:slug>/delete/", category_delete, name="category_delete"),
    path("categories/<slug:slug>/edit/", category_edit, name="category_edit"),
    path("drafts/", DraftEntriesView.as_view(), name="draft_entries"),
    path("entries/all/", AllEntriesView.as_view(), name="all_entries"),
    path("entries/new/", entry_create, name="new_entry"),
    path(
        "feeds/latest-entries/",
        cache_page(settings.CACHE_MIDDLEWARE_SECONDS)(LatestEntriesFeed()),
        name="latest_entries_feed",
    ),
    path("hidden/", HiddenEntriesView.as_view(), name="hidden_entries"),
    path("tag/cloud/", TagCloudView.as_view(), name="tag_cloud"),
    path("tag/<slug:slug>/", TagDetailView.as_view(), name="tag_detail"),
    path("tag/<slug:slug>/edit/", tag_edit, name="tag_edit"),
    path("<slug:slug>/delete/", entry_delete, name="entry_delete"),
    path("<slug:slug>/edit/", entry_edit, name="entry_edit"),
    path("<int:year>/", EntriesByYearView.as_view(), name="by_year"),
    path("<int:year>/<int:month>/", EntriesByMonthView.as_view(), name="by_month"),
    path(
        "<int:year>/<int:month>/<int:day>/", EntriesByDayView.as_view(), name="by_day"
    ),
    path(
        "<int:year>/<int:month>/<int:day>/<slug:slug>/",
        EntryDetailView.as_view(),
        name="entry_detail",
    ),
]
