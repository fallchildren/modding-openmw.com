from django.conf import settings
from django.urls import path
from django.views.decorators.cache import cache_page

# from django.contrib.auth.decorators import login_required
from .feeds import LatestMediaFeed
from .views import (
    LiveAsciinemaView,
    LiveAudioView,
    LiveMediaTracksView,
    LiveVideoView,
    MediaTrackDetailView,
    create_mediatrack,
    edit_mediatrack,
    live_images,
)

app_name = "media"  # TODO: DRY this
urlpatterns = [
    path("", LiveMediaTracksView.as_view(), name="index"),
    path("asciinema/", LiveAsciinemaView.as_view(), name="live_asciinema"),
    path("audio/", LiveAudioView.as_view(), name="live_audio"),
    path(
        "feeds/latest-media/",
        cache_page(settings.CACHE_MIDDLEWARE_SECONDS)(LatestMediaFeed()),
        name="latest_media_feed",
    ),
    path("images/", live_images, name="live_images"),
    path("video/", LiveVideoView.as_view(), name="live_video"),
    path("new/", create_mediatrack, name="create"),
    path("<slug:slug>/", MediaTrackDetailView.as_view(), name="detail"),
    path("<slug:slug>/edit/", edit_mediatrack, name="edit"),
]
