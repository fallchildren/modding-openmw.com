from django.db.models import Manager


class DraftMediaTrackManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.DRAFT)


class HiddenMediaTrackManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.HIDDEN)


class LiveMediaTrackManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.LIVE)
