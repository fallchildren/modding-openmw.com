from django import forms
from .models import MediaTrack


class MediaTrackForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["media_type"].widget.attrs["class"] = "u-full-width"
        self.fields["other_html"].label = "Other HTML"
        self.fields["other_html"].required = False
        self.fields["status"].widget.attrs["class"] = "u-full-width"

    class Meta:
        exclude = ("author", "pub_date")
        model = MediaTrack

    pub_date_0 = forms.DateField(label="Publish Date", required=True)
    pub_date_1 = forms.TimeField(label="Publish Time", required=True)
