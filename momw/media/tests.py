import pytz

from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, User
from django.shortcuts import reverse
from django.test import RequestFactory, TestCase
from .models import MediaTrack
from .views import LiveAsciinemaView, LiveAudioView, LiveMediaTracksView, LiveVideoView


HTTP_FOUND = 302
HTTP_NOT_FOUND = 404
HTTP_OK = 200
TZ = pytz.timezone(settings.TIME_ZONE)


class SetUpBase:
    def setUp(self):
        super().setUp()
        self.f = RequestFactory()
        self.utoob_vid = "https://www.youtube.com/watch?v=NiwqRSCWw2g"
        # TODO: don't create a user here
        if not User.objects.all():
            self.u = User.objects.create(
                first_name="First Name",
                last_name="Last Name",
                username="first-last-jaja-lala",
                password="12345",
            )
        self.asciinema = MediaTrack.objects.create(
            author=self.u,
            description="DESCRIPTION",
            media_type=MediaTrack.ASCIINEMA,
            date_added=datetime.now(TZ),
            slug="ascii-slug",
            status=MediaTrack.LIVE,
            title="ASCIINEMA TITLE",
            url="asciinema.json",
        )
        self.audio = MediaTrack.objects.create(
            author=self.u,
            description="DESCRIPTION",
            media_type=MediaTrack.AUDIO,
            date_added=datetime.now(TZ),
            slug="audio-slug",
            status=MediaTrack.LIVE,
            title="AUDIO TITLE",
            url="audio.ogg",
        )
        self.video = MediaTrack.objects.create(
            author=self.u,
            description="DESCRIPTION",
            media_type=MediaTrack.VIDEO,
            date_added=datetime.now(TZ),
            slug="video-slug",
            status=MediaTrack.LIVE,
            title="VIDEO TITLE",
            url="video.mp4",
        )
        self.utoob = MediaTrack.objects.create(
            author=self.u,
            description="DESCRIPTION",
            media_type=MediaTrack.VIDEO,
            other_html='<iframe width="560" height="315" src="{}" frameborder="0" allowfullscreen></iframe>'.format(
                self.utoob_vid
            ),
            date_added=datetime.now(TZ),
            slug="utoob-slug",
            status=MediaTrack.LIVE,
            title="UTOOB TITLE",
            url=self.utoob_vid,
        )


class MediaLibSeleniumTests(SetUpBase):
    """
    To use this class, subclass it and the BaseSelenium class from 'utilz' as
    seen below.  Ordering matters:

    class YourFirefoxTestCase(BaseSelenium, YourSeleniumTestCase,
                              MediaLibSeleniumTests, StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.driver = FFDriver()

    Where 'YourSeleniumTestCase' is a class that implements Selenium test methods.
    """

    def test_index(self):
        u = "media:index"
        v = LiveMediaTracksView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        # TODO: check for some text

    def test_live_asciinema(self):
        u = "media:live_asciinema"
        v = LiveAsciinemaView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        # TODO: check for some text

    def test_live_audio(self):
        u = "media:live_audio"
        v = LiveAudioView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        # TODO: check for some text

    # TODO: Feed!

    def test_live_video(self):
        u = "media:live_video"
        v = LiveVideoView.as_view()
        self.check_http_status_code(u, v)
        self.check_for_500(u)
        # TODO: check for some text

    # TODO: media:create

    # TODO: media:detail

    # TODO: media:edit


# TODO: many if not all of the below are obsoleted by the above selenium tests
class MediaLibTestCase(SetUpBase, TestCase):
    def test_mediatrack_get_absolute_url(self):
        self.assertIsInstance(self.asciinema.get_absolute_url(), str)

    def test_mediatrack_audio_html(self):
        h = """
<audio src="audio.ogg" controls>
 <a href="{}">Download</a>
</audio>""".format(
            self.audio.url
        )
        self.assertEqual(self.audio.audio_html(), h)

    def test_mediatrack_asciinema_js(self):
        j = """asciinema.player.js.CreatePlayer(
'player-container',
'{0}',
{{
  title:      "{1}"
}});""".format(
            self.asciinema.url, self.asciinema.title
        )
        self.assertEqual(self.asciinema.asciinema_js(), j)

    def test_mediatrack_video_html(self):
        h = """
<video width="768" height="432" controls poster="">
  <source src="{}" type="video/mp4">
Your browser does not support the video tag, so you can't watch this.
</video>""".format(
            self.utoob_vid
        )
        self.assertEqual(self.utoob.video_html(), h)

    def test_live_mediatrack_asciinema_is_viewable(self):
        r = self.f.get(self.asciinema.get_absolute_url())
        r.user = AnonymousUser()
        response = LiveAudioView.as_view()(request=r, slug=self.asciinema.slug)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_live_mediatrack_audio_is_viewable(self):
        r = self.f.get(self.audio.get_absolute_url())
        r.user = AnonymousUser()
        response = LiveAudioView.as_view()(request=r, slug=self.audio.slug)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_live_mediatrack_video_is_viewable(self):
        r = self.f.get(self.video.get_absolute_url())
        r.user = AnonymousUser()
        response = LiveAudioView.as_view()(request=r, slug=self.video.slug)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mediatrack_index(self):
        r = self.f.get(reverse("media:index"))
        r.user = AnonymousUser()
        response = LiveMediaTracksView.as_view()(request=r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mediatrack_asciinema_index(self):
        r = self.f.get(reverse("media:live_asciinema"))
        r.user = AnonymousUser()
        response = LiveAsciinemaView.as_view()(request=r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mediatrack_audio_index(self):
        r = self.f.get(reverse("media:live_audio"))
        r.user = AnonymousUser()
        response = LiveAudioView.as_view()(request=r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mediatrack_video_index(self):
        r = self.f.get(reverse("media:live_video"))
        r.user = AnonymousUser()
        response = LiveVideoView.as_view()(request=r)
        self.assertEqual(response.status_code, HTTP_OK)

    # TODO: create, destroy, and update views
