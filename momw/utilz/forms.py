from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(
        max_length=100,
        help_text="100 Characters Maximum.",
        error_messages={"required": "You must provide a name."},
    )
    email = forms.EmailField(
        error_messages={"required": "Please enter a valid email address."}
    )
    message = forms.CharField(
        widget=forms.Textarea,
        error_messages={"required": "You must leave a message to be sent."},
    )
