from django import template

register = template.Library()


@register.filter(name="times")
def times(number):
    if number:
        return range(1, number + 1)
    else:
        return [0, 0]
