import os
import subprocess

from django.conf import settings


def _get_sha():
    old_cwd = os.getcwd()
    os.chdir(os.path.join(settings.BASE_DIR, ".."))
    try:
        s = subprocess.Popen(["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE)
        s_out = s.communicate()[0]
        os.chdir(old_cwd)
        return s_out.decode().strip("\n")
    except FileNotFoundError:
        return "No Version"


def _get_tag():
    old_cwd = os.getcwd()
    os.chdir(os.path.join(settings.BASE_DIR, ".."))
    try:
        t = subprocess.Popen(["git", "describe", "--tags"], stdout=subprocess.PIPE)
        t_out = t.communicate()[0]
        os.chdir(old_cwd)
        return t_out.decode().strip("\n")
    except FileNotFoundError:
        return "No Tag"


if os.system("git diff-index --quiet HEAD") == 0:
    APP_SHA = _get_sha()
else:
    APP_SHA = _get_sha() + "-dev"

GIT_TAG = _get_tag()


def is_tag_release():
    if GIT_TAG and "-" not in GIT_TAG:
        return True
    return False
