from django.db.models import Manager


class EnabledTagManager(Manager):
    def all(self):
        return super().all().filter(is_enabled=True)


class DraftThingManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.DRAFT)


class HiddenThingManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.HIDDEN)


class LiveThingManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=self.model.LIVE)
