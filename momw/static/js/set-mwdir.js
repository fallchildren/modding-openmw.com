/*
  momw - set-mwdir.js
  Copyright (C) 2023  MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const storageKeyMwDir = "mwDir";

function MwDirSetup() {
    const defaultWindows = "C:\\games\\OpenMWMods\\ModdingResources\\Morrowind\\Data Files";
    const defaultMacOs = "/Users/username/games/OpenMWMods/ModdingResources/Morrowind/Data Files";
    const defaultLinux = "/home/username/games/OpenMWMods/ModdingResources/Morrowind/Data Files";

    const currentMwDir = localStorage.getItem(storageKeyMwDir);
    const currentSpan = document.getElementById("currentMwDir");
    const inputBox = document.getElementById("mwdir");
    const setBtn = document.getElementById("set-mwdir");

    const dataPathsCode = document.getElementById("extra-cfg-o");

    const curValSpan = document.getElementById("mwDir");
    if (curValSpan)
        if (currentMwDir) {
            curValSpan.textContent = currentMwDir;
        } else {
            curValSpan.textContent = "None";
        }

    if ((currentMwDir) && (currentSpan))
        currentSpan.textContent = currentMwDir;

    if (inputBox)
        inputBox.addEventListener("keydown", function (e) {
            if (e.key === "Enter") {
                setMwDir(inputBox.value);
            }
        });

    if (setBtn)
        setBtn.addEventListener("click", function () {
            setMwDir(inputBox.value);
        });

    if (currentMwDir)
        if (dataPathsCode)
            dataPathsCode.textContent = dataPathsCode.textContent
                .replaceAll(defaultMacOs, currentMwDir)
                .replaceAll(defaultWindows, currentMwDir)
                .replaceAll(defaultLinux, currentMwDir);
}

function setMwDir(str) {
    const currentSpan = document.getElementById("currentMwDir");
    if (str === "") {
        localStorage.removeItem(storageKeyMwDir);
        currentSpan.textContent = "None";
    } else {
        localStorage.setItem(storageKeyMwDir, str);
        currentSpan.textContent = str;
    }
}

MwDirSetup();
