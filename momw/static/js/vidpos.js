/*
  momw - vidpos.js
  Copyright (C) 2020  Hristos N. Triantafillou

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var params = new URLSearchParams(window.location.search);
var video = document.getElementsByTagName("video")[0];
var linkButton = document.getElementById("get-link");
var timeLinks = document.getElementsByClassName("time-link");

function giveLink() {
    var urlToGive = window.location.protocol
        + "//"
        + window.location.host
        + window.location.pathname
        + "?t="
        + Math.floor(video.currentTime);

    // Create an ad hoc text area
    var textArea = document.createElement("textarea");

    // Set the value of the ad hoc text area to the URL we are giving
    textArea.value = urlToGive;

    // Append the ad hoc text area to the document's body
    document.body.appendChild(textArea);

    // Select the text (e.g. highlight, mark it)
    textArea.select();

    // Copy it to the user's clipboard
    document.execCommand("copy");

    // Clean up the ad hoc text area
    document.body.removeChild(textArea);

    return urlToGive;
}

function quickSeekListener() {
    quickSeek(this);
}

function quickSeek(timeLink) {
    var time = timeLink.getAttribute("data-time");
    video.currentTime = time;
    video.scrollIntoView({block: "start", behavior: "smooth"});
}

function init() {
    linkButton.addEventListener("click", giveLink);
    for (c = 0; c < timeLinks.length; c++) {
        timeLinks[c].addEventListener("click", quickSeekListener);
    }
}

function SeekVideo() {
     var time = params.get("t");

    if (time)
        video.currentTime = time;
}

SeekVideo();
init();
