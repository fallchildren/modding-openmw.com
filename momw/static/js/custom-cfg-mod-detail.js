/*
  momw - custom-cfg.js
  Copyright (C) 2023  MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const storageKeyInstalledMods = "installedMods";

function InstalledModsSetup() {
    const modDetailBtn = document.getElementById("moddetail-customcfg");
    const rmTxt = "Remove from cfg";

    function getInstalled() {
        return JSON.parse(localStorage.getItem(storageKeyInstalledMods));
    }

    function setInstalled(installd, slug, txt) {
        if (installd === null) {
            // This is the first mod to be setup
            localStorage.setItem(storageKeyInstalledMods, JSON.stringify(new Array(slug)));
        } else {
            installd.push(slug);
            localStorage.setItem(storageKeyInstalledMods, JSON.stringify(installd));
        }
        if (txt !== null)
            modDetailBtn.textContent = txt;
    }

    function rmInstalled(installd, slug, txt, parentBigCheck) {
        installd.splice(installd.indexOf(slug), 1);
        localStorage.setItem(storageKeyInstalledMods, JSON.stringify(installd));
        if (txt !== null)
            modDetailBtn.textContent = txt;
        if (parentBigCheck !== null)
            parentBigCheck.checked = false;
    }

    // For the mod detail page
    if (modDetailBtn) {
        const modSlug = modDetailBtn.getAttribute("data-mod-name");
        let installed = getInstalled();

        // Initial visit to the page, mod is already setup
        if (installed)
            if (installed.includes(modSlug))
                modDetailBtn.textContent = rmTxt;

        modDetailBtn.addEventListener("click", function() {
            let installed = getInstalled();
            if (installed) {
                // There is an existing setup
                if (installed.includes(modSlug)) {
                    // The mod is already setup, undo that
                    rmInstalled(getInstalled(), modSlug, "Enable for CFG Generator");
                } else {
                    // Add the mod to the setup list
                    setInstalled(getInstalled(), modSlug, rmTxt);
                }
            } else {
                // This is the first mod to be setup
                setInstalled(null, modSlug, rmTxt);
            }
        });
    }
}

InstalledModsSetup();
