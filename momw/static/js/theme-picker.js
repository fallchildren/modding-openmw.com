/*
  momw - theme-picker.js
  Copyright (C) 2020  Hristos N. Triantafillou

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const dark = "dark";
const light = "light";
const prefersDark = window.matchMedia("(prefers-color-scheme: dark)").matches;

var preferDarkModeCss = document.getElementById("prefer-darkmode");

function ThemeSetup() {
    var chosenTheme = localStorage.getItem("chosenTheme");
    var currentTheme;
    var toggleBtn = document.getElementById("toggle-theme");

    if (chosenTheme) {
        if (((chosenTheme === dark) && (!prefersDark))
            || ((chosenTheme === light) && (prefersDark))) {
            currentTheme = chosenTheme;
            setTheme(chosenTheme);
        }
        if (toggleBtn)
            toggleBtn.textContent = "Current theme: " + chosenTheme;
    } else {
        if (prefersDark) {
            currentTheme = dark;
        } else {
            currentTheme = light;
        }
        if (toggleBtn)
            toggleBtn.textContent = "Current theme: " + currentTheme;
    }

    if (toggleBtn)
        toggleBtn.addEventListener("click", toggleTheme);
}

function getCurrentTheme() {
    var chosenTheme = localStorage.getItem("chosenTheme");
    if (chosenTheme)
        return chosenTheme;

    if (prefersDark)
        return dark;

    return light;
}

function darkCssUrl() {
    if (location.hostname === "0.0.0.0") {
        // This is a dev environment; the site is running locally.
        return "/static/css/site-dark.css";
    } else {
        return "/css/site-dark.css";
    }
}

function setTheme(theme) {
    if (theme === dark) {
        if (prefersDark) {
            preferDarkModeCss.href = darkCssUrl();
        } else {
            preferDarkModeCss.media = "";
        }

        localStorage.setItem("chosenTheme", dark);

    } else if (theme === light) {
        if (prefersDark) {
            preferDarkModeCss.href = "";
        } else {
            preferDarkModeCss.media = "(prefers-color-scheme: dark)";
        }
        localStorage.setItem("chosenTheme", light);
    }
}

function toggleTheme() {
    var currentTheme = getCurrentTheme();
    var theme;
    var toggleBtn = document.getElementById("toggle-theme");
    if (currentTheme === dark) {
        setTheme(light);
        theme = light;
    } else if (currentTheme === light) {
        setTheme(dark);
        theme = dark;
    }

    toggleBtn.textContent = "Current theme: " + theme;
}

ThemeSetup();
