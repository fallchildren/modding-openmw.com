#!/bin/bash -e

_hosts="${1}"

if [ -z "${_hosts}" ]; then
    echo PROVIDE HOSTS!
    exit 1
fi

make sync HOSTS="${_hosts}"
SKIPTESTS=true make django-resetdb HOSTS="${_hosts}"
make memcached-restart HOSTS="${_hosts}"
